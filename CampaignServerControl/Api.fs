﻿// CampaignServerControl, API between SturmovikServerControl and SturmovikCampaign
// Copyright (C) 2018 Johann Deneux <johann.deneux@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

namespace CampaignServerControl.Api

open System
open System.Numerics

/// An async computation that is scheduled at a specific time and
/// returns a list of following scheduled async computations.
type ScheduledTask =
    | SomeTask of DateTime * Async<ScheduledTask> * string
    | NoTask
with
    static member SomeTaskNow description t =
        SomeTask(
            DateTime.UtcNow,
            async {
                let! res = t
                return res
            },
            description
        )
    
    member this.ContinueWith(cont) =
        match this with
        | NoTask -> NoTask
        | SomeTask(time, task0, description) ->
            let newTask =
                async {
                    let! res = task0
                    let! res = cont res
                    return res
                }
            SomeTask(
                time,
                newTask,
                description)

/// Append messages to the server controller log.
/// Implemented by the server controller.
type LoggingApi =
    abstract LogError : string -> unit
    abstract LogInfo : string -> unit

/// Identify a player uniquely.
/// Implemented by the server controller.
type PlayerId =
    abstract GetName : unit -> string

/// Identify a team (axis, allies) uniquely.
/// Implemented by the server controller.
type TeamId =
    abstract GetName : unit -> string

/// Interaction with the game server.
/// Implemented by the server controller.
type ServerControlApi =
    abstract SkipMission : Async<unit>
    abstract ServerInput : string -> Async<unit>
    abstract GetPlayerList : Async<PlayerId list>
    abstract GetAxisTeam : unit -> TeamId
    abstract GetAlliesTeam : unit -> TeamId
    abstract MessagePlayer : PlayerId * string list -> Async<unit>
    abstract MessageTeam : TeamId * string list -> Async<unit>
    abstract MessageAll : string list -> Async<unit>
    abstract KickPlayer : PlayerId -> Async<unit>
    abstract BanPlayer : PlayerId * hours:int -> Async<unit>
    abstract TryGetPlayerPin : Guid -> Async<string option>
    abstract TryGetPlayerByUserId : Guid -> Async<PlayerId option>

/// Types used to annotate a map, used by the campaign plugin to provide a representation of the current situation.
module MapGraphics =
    type MapName =
        | Stalingrad
        | Moscow
        | VelikieLuki
        | Kuban

    type TeamColor =
        | Red
        | Gray

    type MapArrow =
        { Start : Vector2
          Tip : Vector2
          Width : Vector2
          Color : Vector3
        }

    type MapIconSymbol =
        | Tank
        | Truck
        | Plane
        | Ship
        | Artillery
        | House
        | Factory
        | Base
        | Airfield
        | Clash
        | Death
        | Exclamation
        | Parachute
        | Sun
        | Moon
        | Cloud
        | Rain

    type MapIcon =
        { Position : Vector2
          Icon : MapIconSymbol
          Label : string option
          Description : string option
          Depth : float32
          Color : TeamColor
        }

    type MapArea =
        { Boundaries : Vector2 list
          Color : Vector3
        }

    type MapHoverNote =
        { Position : Vector2
          Radius : float32
          Text : string
        }

    type MapReportEntryLabelType =
        | ReportEntryIconLabel of MapIconSymbol
        | ReportEntryTextLabel of string

    type MapReportEntry =
        { Label : MapReportEntryLabelType
          Description : string
        }

    type MapAnchoredReport =
        { Position : Vector2
          Radius : float32
          Report : MapReportEntry list
        }

    type MapPackage =
        { Name : MapName
          AnchoredReports : MapAnchoredReport list
          HoverNotes : MapHoverNote list
          SideNotes : MapReportEntry list
          Areas : MapArea list
          Icons : MapIcon list
        }
    with
        static member Default =
            { Name = Stalingrad
              AnchoredReports = []
              HoverNotes = []
              SideNotes = []
              Areas = []
              Icons = []
            }

/// Interaction with the map rendering system.
/// Implement by the server controller.
type ServerRenderingApi =
    abstract SetPackage : MapGraphics.MapPackage -> unit

type SupportApis =
    { Logging : LoggingApi
      ServerControl : ServerControlApi
      MapGraphics : ServerRenderingApi
    }

/// Interface between SturmovikServerControl and the campaign system.
/// Implemented by the campaign system.
type CampaignServerApi =
    /// Called by the server controller to provide the logging and server control APIs
    abstract Init : SupportApis -> unit
    /// Start or resume the campaign system, and return the next scheduled updates
    abstract StartOrResume : config:string -> Choice<ScheduledTask, string>
    /// Reset campaign state and then start the camapign
    abstract Reset : config:string * scenario:string -> Choice<ScheduledTask, string>
    /// Get the planes reserved for a player: Map airfield names to maps from plane model names to quantities
    abstract GetPlayerReservedPlanes : playerName:string -> Async<Map<string, Map<string, float32>>>
    /// Get the number of fresh spawns at the rear airfield: Map coalition to plane type to quantities
    abstract GetPlayerFreshSpawns : playerName:string -> Async<Map<string, Map<string, float32>>>
    /// Get cash reserve of a player. Returns a map from coalition to amount
    abstract GetPlayerCashReserve : playerName:string-> Async<Map<string, float32>>
    /// A player gives one of its reserved planes to another player or to the public
    abstract GiftReservedPlane : giverName:string * recipientGuidAndName:(string * string) option * planeName:string * airfieldName:string -> Async<Result<string, string>>
    /// Request data from the plugin.
    /// This can be used to e.g. expose data about the status of the currently running mission, the campaign...
    abstract GetData : string * string list -> Async<Result<string, string>>
