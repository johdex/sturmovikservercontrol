#!/bin/bash

mkdir -p sturmovikservercontrol-v/SturmovikServerControl/bin

cd SturmovikServerControl/bin
cp -r Release ../../sturmovikservercontrol-v/SturmovikServerControl/bin/
cd ..
cp -r Content Images Scripts ../sturmovikservercontrol-v/SturmovikServerControl/
cd ../SetPassword/bin
cp -r Release ../../sturmovikservercontrol-v/SetPassword
