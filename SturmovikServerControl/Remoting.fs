// SturmovikServerControl Copyright (C) 2016 Johann Deneux <johann.deneux@gmail.com>
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
module SturmovikServerControl.Server

open WebSharper
open System
open System.IO
open SturmovikServerControl.ServerState
open SturmovikServerControl.Database
open System.Collections
open System.Web.Compilation

// Logging
let private logger = NLog.LogManager.GetCurrentClassLogger()
let info src m = logger.Info(sprintf "%s: %s" src m)
let verbose src m = logger.Debug(sprintf "%s: %s" src m)
let error src m = logger.Error(sprintf "%s: %s" src m)

[<Rpc>]
let isLoggedAsAdmin() =
    let context = WebSharper.Web.Remoting.GetContext()
    async {
        let! user = context.UserSession.GetLoggedInUser()
        return ServerState.isAdmin(user)
    }

[<Rpc>]
let printInfo msg =
    async {
        info "Server.printInfo" msg
        return ()
    }

[<Rpc>]
let getStatus(instance) =
    async {
        match queues.TryGetValue(instance) with
        | true, queue ->
            try
                let! status = queue.Run(lazy queue.Client.ServerStatus())
                info "getStatus" (sprintf "Status response: %A" status)
                return
                    match status with
                    | Some 1 -> "Running a mission"
                    | Some 7 -> "Not running a mission"
                    | Some _ -> "Has encountered an error"
                    | None -> "Unreachable"
            with
            | e ->
                error "getStatus" e.Message
                return "Failed to get status"
        | false, _ ->
            error "getStatus" (sprintf "No such server instance %s" instance)
            return "Server instance not found"
    }

[<Rpc>]
let getSps(instance) =
    async {
        match queues.TryGetValue(instance) with
        | true, queue ->
            try
                let! sps = queue.Run(lazy queue.Client.GetSPS())
                info "getSps" (sprintf "SPS: %A" sps)
                match sps with
                | Some sps ->
                    return sps.Current
                | None ->
                    return -1.0f
            with
            | e ->
                error "getSps" e.Message
                return -1.0f
        | false, _ ->
            error "getStatus" (sprintf "No such server instance %s" instance)
            return -1.0f
    }

[<Rpc>]
let getPlayerList(instance) =
    async {
        match queues.TryGetValue(instance) with
        | true, queue ->
            let! players = queue.Run(lazy queue.Client.GetPlayerList())
            match players with
            | Some(Some(players)) ->
                verbose "Server.getPlayerList" (sprintf "%A" players)
                let players =
                    players
                    |> Array.map(fun data -> data.Name, data.PlayerId)
                    |> List.ofArray
                return players
            | _ ->
                info "Server.getPlayerList" "Failed to retrieve player list"
                return []
        | false, _ ->
            error "Server.getPlayerList" (sprintf "No such server instance %s" instance)
            return []
    }

[<Rpc>]
let scheduleMission(instance, time, filename) =
    let context = WebSharper.Web.Remoting.GetContext()
    async {
        match queues.TryGetValue(instance) with
        | true, queue ->
            info "Server.scheduleMission" (sprintf "time = %s, filename = %s" time filename)
            let! user = context.UserSession.GetLoggedInUser()
            return
                if isAdmin(user) then
                    let isInvalid =
                        System.IO.Path.GetInvalidPathChars()
                        |> Seq.exists (fun c -> filename.Contains(string c))
                    if not isInvalid then
                        scheduler.TryPost(instance, time, Schedule.OpenSDS(filename), sprintf "Open SDS '%s'" filename)
                    else
                        info "Server.scheduledMission" "Invalid char in path/filename"
                        false
                else
                    info "Server.scheduledMission" "User not admin"
                    false
        | false, _ ->
            error "Server.scheduleMission" (sprintf "No such server instance %s" instance)
            return false
    }

[<Rpc>]
let scheduleBroadcast(instance, time, message) =
    let context = WebSharper.Web.Remoting.GetContext()
    async {
        match queues.TryGetValue(instance) with
        | true, queue ->
            info "Server.scheduleBroadcast" (sprintf "time = %s, message = %s" time message)
            let! user = context.UserSession.GetLoggedInUser()
            return
                if isAdmin(user) then
                    scheduler.TryPost(instance, time, Schedule.Broadcast(message), "Broadcast")
                else
                    info "Server.scheduleBroadcast" "User not admin"
                    false
        | false, _ ->
            error "Server.scheduleBroadcast" (sprintf "No such server instance %s" instance)
            return false
    }

[<Rpc>]
let scheduleShutdown(instance, time, message) =
    let context = WebSharper.Web.Remoting.GetContext()
    async {
        match queues.TryGetValue(instance) with
        | true, queue ->
            info "Server.scheduleShutdown" (sprintf "time = %s, message = %s" time message)
            let! user = context.UserSession.GetLoggedInUser()
            return
                if isAdmin(user) then
                    scheduler.TryPost(instance, time, Schedule.Shutdown(message), "Shut down")
                else
                    info "Server.scheduleShutdown" "User not admin"
                    false
        | false, _ ->
            error "Server.scheduleShutdown" (sprintf "No such server instance %s" instance)
            return false
    }

[<Rpc>]
let scheduleCampaignReset(instance, time, scenario) =
    let context = WebSharper.Web.Remoting.GetContext()
    async {
        match queues.TryGetValue(instance) with
        | true, queue ->
            info "Server.scheduleCampaignReset" (sprintf "time = %s" time)
            let! user = context.UserSession.GetLoggedInUser()
            return
                if isAdmin(user) then
                    scheduler.TryPost(instance, time, Schedule.CampaignReset scenario, "Campaign reset")
                else
                    info "Server.scheduleCampaignReset" "User not admin"
                    false
        | false, _ ->
            error "Server.scheduleCampaignReset" (sprintf "No such server instance %s" instance)
            return false
    }

[<Rpc>]
let scheduleCampaign(instance, time) =
    let context = WebSharper.Web.Remoting.GetContext()
    async {
        match queues.TryGetValue(instance) with
        | true, queue ->
            info "Server.scheduleCampaign" (sprintf "time = %s" time)
            let! user = context.UserSession.GetLoggedInUser()
            return
                if isAdmin(user) then
                    scheduler.TryPost(instance, time, Schedule.CampaignInit, "Continue campaign")
                else
                    info "Server.scheduleCampaign" "User not admin"
                    false
        | false, _ ->
            error "Server.scheduleCampaign" (sprintf "No such server instance %s" instance)
            return false
    }

[<Rpc>]
let getNow() =
    async {
        return DateTime.UtcNow.ToString(@"yyyy\/MM\/dd HH:mm")
    }

[<Rpc>]
let getSchedule() =
    let timeToString (time : DateTime) =
        sprintf "%s %s" (time.ToString(@"yyyy\/MM\/dd")) (time.ToString("HH:mm"))
    async {
        let actions = scheduler.Actions
        let actions =
            actions
            |> List.map (fun (instance, idx, time, desc) -> (instance, idx), timeToString time, sprintf "%s: %s" instance desc)
        return actions
    }

[<Rpc>]
let getRunning() =
    async {
        let actions = scheduler.Running
        return actions
    }

[<Rpc>]
let cancelMission(idx) =
    let context = WebSharper.Web.Remoting.GetContext()
    async {
        let! user = context.UserSession.GetLoggedInUser()
        if isAdmin(user) then
            scheduler.Cancel(idx)
        else
            info "Server.scheduledMission" "User not admin"
    }

[<Rpc>]
let sendServerInput(instance, serverInput) =
    let context = WebSharper.Web.Remoting.GetContext()
    async {
        match queues.TryGetValue(instance) with
        | true, queue ->
            let! user = context.UserSession.GetLoggedInUser()
            if isAdmin(user) then
                let! response = queue.Run(lazy queue.Client.ServerInput(serverInput))
                verbose "Server.sendServerInput" (sprintf "ServerInput response: %A" response)
            else
                info "Server.sendServerInput" "User not admin"
        | false, _ ->
            error "Server.sendServerInput" (sprintf "No such server instance %s" instance)
    }

[<Rpc>]
let sendOffLoadServerInput(instance) =
    let context = WebSharper.Web.Remoting.GetContext()
    async {
        match queues.TryGetValue(instance), instances.TryGetValue(instance) with
        | (true, queue), (true, instance) ->
            match instance.OffLoadServerInput with
            | Some serverInput ->
                let! user = context.UserSession.GetLoggedInUser()
                if isAdmin(user) then
                    let! response = queue.Run(lazy queue.Client.ServerInput(serverInput))
                    verbose "Server.sendOffLoadServerInput" (sprintf "ServerInput response: %A" response)
                else
                    info "Server.sendOffLoadServerInput" "User is not admin"
            | None ->
                info "Server.sendOffLoadServerInput" "OffLoadServerInput not set in the configuration"
        | (false, _), _
        | _, (false, _) ->
            error "Server.sendOffLoadServerInput" (sprintf "No such server instance %s" instance)
    }

[<Rpc>]
let loginAdmin(password) =
    let context = WebSharper.Web.Remoting.GetContext()
    async {
        if config.CheckAdminPassword(password) then
            do! context.UserSession.LoginUser("admin")
            return true
        else
            return false
    }

[<Rpc>]
let logout() =
    let context = WebSharper.Web.Remoting.GetContext()
    context.UserSession.Logout()

[<Rpc>]
let loginPlayer(player, pinCode : string) =
    let context = WebSharper.Web.Remoting.GetContext()
    async {
        let! user = context.UserSession.GetLoggedInUser()
        if player = "admin" then
            info "Server.loginPlayer" "attempt to log in as admin, not allowed"
            return false
        else
            match Database.tryGetGuidByName(player) with
            | Some guid ->
                let realPinCode = Database.tryGetUserPinCode(guid)
                if user = Some "admin" then
                    info "Server.loginPlayer" (sprintf "admin allowed to act as %s" player)
                    return true
                elif realPinCode = Some (pinCode.ToUpperInvariant()) then
                    info "Server.loginPlayer" (sprintf "valid pin for %s" player)
                    do! context.UserSession.LoginUser(player)
                    return true
                else
                    info "Server.loginPlayer" (sprintf "invalid pin for %s" player)
                    return false
            | None ->
                info "Server.loginPlayer" (sprintf "invalid login %s" player)
                return false
    }

[<Rpc>]
let loggedInAsPlayer() =
    let context = WebSharper.Web.Remoting.GetContext()
    async {
        let! user = context.UserSession.GetLoggedInUser()
        match user with
        | None | Some "admin" ->
            return None
        | Some _ ->
            return user
    }

[<Rpc>]
let checkBans() =
    async {
        let now = DateTime.UtcNow
        let banned =
            try
                getBannedPlayerIds now
            with
            e ->
                error "Server.checkBans" (sprintf "Db query failed %s" e.Message)
                Set.empty
        for queue in queues.Values do
            let! players = queue.Run(lazy queue.Client.GetPlayerList())
            match players with
            | Some(Some(players)) ->
                try
                    players
                    |> Array.map (fun data ->
                        {
                            Database.PlayerData.Name = data.Name
                            PlayerId = Guid(data.PlayerId)
                            ProfileId = Guid(data.ProfileId)
                        })
                    |> List.ofArray
                    |> updateNames
                with
                | e ->
                    error "Server.checkBans" (sprintf "Db names update failed %s" e.Message)
                for player in players do
                    if banned.Contains(player.PlayerId) then
                        let! s = queue.Run(lazy queue.Client.BanPlayer(player.ClientId))
                        if Option.isSome s then
                            info "CheckBans" (sprintf "Banned '%s'" player.Name)
                        else
                            error "CheckBans" (sprintf "Failed to ban '%s'" player.Name)
            | None ->
                () // Couldn't speak to server, maybe it's not up. Not an error.
            | Some None ->
                // Server replied with an error status message. It's worth reporting.
                error "CheckBans" "Failed to retrieve player list"
    }

[<Rpc>]
let banUser(playerId : string, duration : int, comment : string) =
    let context = WebSharper.Web.Remoting.GetContext()
    async {
        let! user = context.UserSession.GetLoggedInUser()
        if isAdmin(user) then
            let release = DateTime.UtcNow.AddHours(float duration)
            try
                banUser(playerId, release, comment)
            with
            | e -> error "Server.banUser" (sprintf "Db update failed: %s" e.Message) 
        else
            info "Server.banUser" "User is not admin"
    }

[<Rpc>]
let unBanUser(playerId : string) =
    let context = WebSharper.Web.Remoting.GetContext()
    async {
        let! user = context.UserSession.GetLoggedInUser()
        if isAdmin(user) then
            try
                unbanUserByPlayerId playerId
            with
            | e -> error "Server.unBanUser" (sprintf "Db update failed: %s" e.Message)
        else
            info "Server.unBanUser" "User is not admin"
    }

[<Rpc>]
let searchUser(playerId : string, playerName : string) =
    let context = WebSharper.Web.Remoting.GetContext()
    async {
        let! user = context.UserSession.GetLoggedInUser()
        if isAdmin(user) then
            let byPlayerId =
                if System.String.IsNullOrWhiteSpace(playerId) then
                    []
                else
                    try
                        searchUserByPlayerId playerId
                    with
                    | e ->
                        error "Server.searchUser" (sprintf "Db query failed: %s" e.Message)
                        []
            let byName =
                if System.String.IsNullOrWhiteSpace(playerName) then
                    []
                else
                    try
                        searchUserByName playerName
                    with
                    | e ->
                        error "Server.searchUser" (sprintf "Db query failed: %s" e.Message)
                        []
            let res =
                byPlayerId @ byName
                |> List.map (fun (x, y, z) -> (x.ToString(@"yyyy\/MM\/dd HH:mm"), y, string z))
            return res
        else
            info "Server.searchUser" "User is not admin"
            return []
    }


[<Rpc>]
let searchUserName(playerName : string) =
    async {
        let byName =
            if System.String.IsNullOrWhiteSpace(playerName) then
                []
            else
                try
                    searchUserByName playerName
                with
                | e ->
                    error "Server.searchUser" (sprintf "Db query failed: %s" e.Message)
                    []
        let res =
            match byName with
            | [] -> None
            | [x] -> Some x
            | _ :: _ ->
                byName
                |> List.maxBy (fun (date, _, _) -> date)
                |> Some
            |> Option.map (fun (_, name, _) -> name)
        return res
    }

[<Rpc>]
let requestTermination() =
    let context = WebSharper.Web.Remoting.GetContext()
    async {
        let! user = context.UserSession.GetLoggedInUser()
        if isAdmin(user) then
            ServerState.requestTermination()
        else
            info "Server.requestTermination" "User is not admin"
    }

[<Rpc>]
let getCashReserve(instance, playerName) =
    let context = WebSharper.Web.Remoting.GetContext()
    async {
        let! user = context.UserSession.GetLoggedInUser()
        match user with
        | Some x when isAdmin(user) || x = playerName ->
            match plugins.TryGetValue(instance) with
            | true, (plugin, _, _) ->
                let! reserve = plugin.GetPlayerCashReserve(playerName)
                return reserve
            | false, _ ->
                return Map.empty
        | _ ->
            return Map.empty
    }

[<Rpc>]
let getReservedPlanes(instance, playerName) =
    let context = WebSharper.Web.Remoting.GetContext()
    async {
        let! user = context.UserSession.GetLoggedInUser()
        match user with
        | Some x when isAdmin(user) || x = playerName ->
            match plugins.TryGetValue(instance) with
            | true, (plugin, _, _) ->
                let! reserved = plugin.GetPlayerReservedPlanes(playerName)
                logger.Debug(sprintf "Reserved planes for %s on %s is %A" playerName instance reserved)
                return reserved
            | false, _ ->
                return Map.empty
        | _ ->
            return Map.empty
    }

[<Rpc>]
let getFreshSpawns(instance, playerName) =
    let context = WebSharper.Web.Remoting.GetContext()
    async {
        let! user = context.UserSession.GetLoggedInUser()
        match user with
        | Some x when isAdmin(user) || x = playerName ->
            match plugins.TryGetValue(instance) with
            | true, (plugin, _, _) ->
                let! spawns = plugin.GetPlayerFreshSpawns(playerName)
                logger.Debug(sprintf "Fresh spawns for %s on %s is %A" playerName instance spawns)
                return spawns
            | false, _ ->
                return Map.empty
        | _ ->
            return Map.empty
    }

[<Rpc>]
let giftPlane(instance, giver, recipient, airfield, plane) =
    let context = WebSharper.Web.Remoting.GetContext()
    async {
        let! user = context.UserSession.GetLoggedInUser()
        match user with
        | Some x when isAdmin(user) || x = giver ->
            match plugins.TryGetValue(instance) with
            | true, (plugin, _, _) ->
                let recipientIds =
                    match recipient with
                    | Some recipientName ->
                        try
                            searchUserByName recipientName
                        with
                        | e ->
                            error "Server.searchUser" (sprintf "Db query failed: %s" e.Message)
                            []
                        |> List.sortByDescending (fun (date, _, _) -> date)
                        |> List.tryHead
                        |> Option.map (fun (_, name, guid) -> Ok(Some(string guid, name)))
                        |> Option.defaultValue (Error "recipient not found")
                    | None ->
                        Ok None
                match recipientIds with
                | Ok recipientIds ->
                    let! status = plugin.GiftReservedPlane(giver, recipientIds, plane, airfield)
                    logger.Debug(sprintf "%s: Gift from %s to %s of a %s at %s" instance giver (recipient |> Option.defaultValue "all") plane airfield)
                    match status with
                    | Ok msg -> return Choice1Of2 msg
                    | Error msg -> return Choice2Of2 msg
                | Error msg ->
                    return Choice2Of2 msg
            | false, _ ->
                return Choice2Of2 (sprintf "No active campaign on %s" instance)
        | _ ->
            return Choice2Of2 "Must log in first"
    }
