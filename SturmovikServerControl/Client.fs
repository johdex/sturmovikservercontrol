// SturmovikServerControl Copyright (C) 2016 Johann Deneux <johann.deneux@gmail.com>
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

namespace SturmovikServerControl

open System
open System.Text
open WebSharper
open WebSharper.JavaScript
open WebSharper.UI.Next
open WebSharper.UI.Next.Client
open WebSharper.UI.Next.Html
open WebSharper.Leaflet

[<JavaScript>]
module Client =
    open WebSharper.Sitelets

    let formRow xs =
        divAttr [ attr.``class`` "form-group row" ] xs

    let formCell n =
        divAttr [ attr.``class`` <| sprintf "col-sm-%d" n ]

    let AdminLink(link, txt) =
        Doc.Async(
            async {
                let! isAdmin = Server.isLoggedAsAdmin()
                if isAdmin then
                    return li [ aAttr [ attr.href link ] [ text txt ] ] :> Doc
                else
                    return Doc.Empty
            })

    let Login() =
        let vPassword = Var.Create ""
        let vStatus = Var.Create None
        let submit =
            Doc.Button "Log in"
                [
                    attr.``type`` "submit"
                    attr.``class`` "btn btn-primary"
                ]
                (fun () ->
                    async {
                        let! ok = Server.loginAdmin(vPassword.Value)
                        vStatus.Value <-
                            if ok then
                                Some "OK"
                            else
                                Some "Failed"
                    }
                    |> Async.Start
                )
        let statusView =
            vStatus.View
            |> View.Map (fun msg ->
                match msg with
                | None ->
                    Doc.Empty
                | Some "OK" ->
                    divAttr [ attr.``class`` "alert alert-success" ; Attr.Create "role" "alert" ] [ text "OK" ] :> Doc
                | Some msg ->
                    divAttr [ attr.``class`` "alert alert-danger" ; Attr.Create "role" "alert" ] [ text msg ] :> Doc
            )
        div [
            formAttr [ attr.``class`` "form-inline" ]  [
                labelAttr [ attr.``for`` "idPassword" ; attr.``class`` "form-control-label" ] [ text "Password" ]
                Doc.Input [ attr.``type`` "password" ; attr.``class`` "form-control" ; attr.id "idPassword" ; attr.placeholder "Password" ] vPassword
                submit
            ]
            Doc.EmbedView statusView
        ]

    let playerSearch =
        let vPattern = Var.Create ""
        let vGuid = Var.Create ""
        let vResults = Var.Create []

        let resultsView =
            vResults.View
            |> View.Map (fun results ->
                tableAttr [ attr.``class`` "table" ] [
                    thead [
                        tr [
                            th [ text "Date" ]
                            th [ text "Name" ]
                            th [ text "Guid" ]
                            th [ text "Actions" ]
                        ]
                    ]
                    tbody [
                        for date, name, guid in results do
                            yield tr [
                                td [ text date ]
                                td [ text name ]
                                td [ text guid ]
                                td [
                                    Doc.Button "Ban day" [ attr.``class`` "btn btn-secondary" ] (fun () ->
                                        async {
                                            do! Server.banUser(guid, 24, "Manual ban for a day")
                                        }
                                        |> Async.Start
                                    )
                                    Doc.Button "Ban month" [ attr.``class`` "btn btn-secondary" ] (fun () ->
                                        async {
                                            do! Server.banUser(guid, 24 * 31, "Manual ban for a month")
                                        }
                                        |> Async.Start
                                    )
                                    Doc.Button "Ban century" [ attr.``class`` "btn btn-secondary" ] (fun () ->
                                        async {
                                            do! Server.banUser(guid, 24 * 365 * 100, "Manual ban for 100 years")
                                        }
                                        |> Async.Start
                                    )
                                    Doc.Button "Clear" [ attr.``class`` "btn btn-secondary" ] (fun () ->
                                        async {
                                            do! Server.unBanUser(guid)
                                        }
                                        |> Async.Start
                                    )
                                ]
                            ] :> Doc
                    ]
                ]
            )
        div [
            formAttr [ attr.``class`` "form-inline" ]  [
                labelAttr [ attr.``for`` "idPattern" ; attr.``class`` "form-control-label" ] [ text "Pattern" ]
                Doc.Input [ attr.``class`` "form-control" ; attr.id "idPattern" ; attr.placeholder "playername" ] vPattern
                labelAttr [ attr.``for`` "idGuid" ; attr.``class`` "form-control-label" ] [ text "Guid" ]
                Doc.Input [ attr.``class`` "form-control" ; attr.id "idGuid" ; attr.placeholder "39cee87c-0783-4f28-b1ce-ce805d44c021" ] vGuid
                Doc.Button "Search" [ attr.``type`` "submit"; attr.``class`` "btn btn-primary" ] (fun () ->
                    async {
                        let! results = Server.searchUser(vGuid.Value, vPattern.Value)
                        vResults.Value <- results
                    } |> Async.Start)
            ]
            Doc.EmbedView resultsView
        ]

    let StatusOfInstance(instance) =
        let vStatus = Var.Create "??"
        let vSps = Var.Create "??"
        let vPlayers = Var.Create []
        let update =
            async {
                let! sps = Server.getSps(instance)
                vSps.Value <- sprintf "%3.1f" sps
                let! status = Server.getStatus(instance)
                vStatus.Value <- status
                let! players = Server.getPlayerList(instance)
                vPlayers.Value <- players
            }

        let isAdmin =
            View.ConstAsync (Server.isLoggedAsAdmin())

        let offLoad =
            isAdmin
            |> View.Map (fun isAdmin ->
                if isAdmin then
                    Doc.Button "Reduce load" [ attr.``class`` "btn btn-secondary" ] (fun () ->
                        Async.Start <| Server.sendOffLoadServerInput(instance)
                    ) :> Doc
                else
                    Doc.Empty)
            |> Doc.EmbedView

        let playersView =
            vPlayers.View
            |> View.Map2 (fun isAdmin players ->
                tableAttr [ attr.``class`` "table" ] [
                    thead [
                        tr [
                            yield th [ text "Player name" ] :> Doc
                            if isAdmin then
                                yield th [ text "Ban" ] :> Doc
                        ]
                    ]
                    tbody [
                        for playerName, playerId in players do
                            yield tr [
                                yield td [ text playerName ] :> Doc
                                if isAdmin then
                                    yield td [
                                        Doc.Button "Ban day" [ attr.``class`` "btn btn-secondary" ] (fun () ->
                                            async {
                                                do! Server.banUser(playerId, 24, "Manual ban for a day")
                                            }
                                            |> Async.Start
                                        )
                                        Doc.Button "Ban month" [ attr.``class`` "btn btn-secondary" ] (fun () ->
                                            async {
                                                do! Server.banUser(playerId, 24 * 31, "Manual ban for a month")
                                            }
                                            |> Async.Start
                                        )
                                        Doc.Button "Ban century" [ attr.``class`` "btn btn-secondary" ] (fun () ->
                                            async {
                                                do! Server.banUser(playerId, 24 * 365 * 100, "Manual ban for 100 years")
                                            }
                                            |> Async.Start
                                        )
                                    ] :> Doc
                            ] :> Doc
                    ]
                ]
            ) isAdmin
            |> Doc.EmbedView

        async {
            while true do
                do! update
                do! Async.Sleep 60000
        } |> Async.Start

        div [
            p [ text (sprintf "DServer instance: %s" instance) ]
            p [ text "Status: " ; textView vStatus.View ]
            p [ text "Simulations per second: " ; textView vSps.View ]
            offLoad
            playersView
        ]

    let Status(instances : string[]) =
        let isAdmin =
            View.ConstAsync (Server.isLoggedAsAdmin())
        let playerSearch =
            isAdmin
            |> View.Map (fun isAdmin ->
                if isAdmin then
                    playerSearch :> Doc
                else
                    Doc.Empty)
            |> Doc.EmbedView
        div [
            yield playerSearch
            for instance in instances do
                yield StatusOfInstance(instance) :> Doc
        ]

    type ScheduledEvent =
        | LoadMission
        | Shutdown
        | Broadcast
        | ResetCampaign
        | StartCampaign

    let Schedule(instances : string[]) =
        let vInstance = Var.Create instances.[0]
        let vTime = Var.Create ""
        let vMission = Var.Create ""
        let vReason = Var.Create ""
        let vBroadcast = Var.Create ""
        let vScenario = Var.Create "MoscowWinter"
        let vStatus = Var.Create ""
        let vList = Var.Create []
        let vRunning = Var.Create []
        let vChoice = Var.Create LoadMission
        let submit = Doc.Button "Add" [ attr.``type`` "submit" ; attr.``class`` "btn btn-primary" ] (fun () ->
            async {
                let time = vTime.Value
                let instance = vInstance.Value
                match vChoice.Value with
                | ScheduledEvent.LoadMission ->
                    let mission = vMission.Value
                    let! ok = Server.scheduleMission(instance, time, mission)
                    vStatus.Value <-
                        if ok then
                            ""
                        else
                            "Failed to schedule mission"
                | ScheduledEvent.Broadcast ->
                    let message = vBroadcast.Value
                    let! ok = Server.scheduleBroadcast(instance, time, message)
                    vStatus.Value <-
                        if ok then
                            ""
                        else
                            "Failed to schedule broadcast"
                | ScheduledEvent.Shutdown ->
                    let message = vReason.Value
                    let! ok = Server.scheduleShutdown(instance, time, message)
                    vStatus.Value <-
                        if ok then
                            ""
                        else
                            "Failed to schedule shutdown"
                | ScheduledEvent.ResetCampaign ->
                    let scenario = vScenario.Value
                    let! ok = Server.scheduleCampaignReset(instance, time, scenario)
                    vStatus.Value <-
                        if ok then
                            ""
                        else
                            "Failed to schedule campaign state reset"
                | ScheduledEvent.StartCampaign ->
                    let! ok = Server.scheduleCampaign(instance, time)
                    vStatus.Value <-
                        if ok then
                            ""
                        else
                            "Failed to schedule campaign engine start"
                // Update view of scheduled and running tasks
                let! tasks = Server.getSchedule()
                vList.Value <- tasks
                let! running = Server.getRunning()
                vRunning.Value <- running
            }
            |> Async.Start
        )
        let restart = Doc.Button "Restart" [ attr.``class`` "btn btn-secondary" ] (fun () ->
            async {
                do! Server.requestTermination()
            }
            |> Async.Start
        )
        let getServerTime = Doc.Button "Get server time" [ attr.``class`` "btn btn-secondary" ] (fun () ->
            async {
                let! now = Server.getNow()
                vTime.Value <- now
            }
            |> Async.Start
        )
        let getList = Doc.Button "Refresh" [ attr.``class`` "btn btn-secondary" ] (fun () ->
            async {
                // Update view of scheduled and running tasks
                let! tasks = Server.getSchedule()
                vList.Value <- tasks
                let! running = Server.getRunning()
                vRunning.Value <- running
            }
            |> Async.Start
        )
        let taskView =
            vList.View
            |> Doc.BindView (fun tasks ->
                tableAttr [ attr.``class`` "table" ] [
                    thead [
                        tr [
                            th [ text "Time" ]
                            th [ text "Event"]
                            th []
                        ]
                    ]
                    tbody [
                        for (instance, idx), time, mission in tasks do
                            let cancel = Doc.Button "X" [] (fun () ->
                                async {
                                    do! Server.cancelMission(instance, idx)
                                    do! Async.Sleep(1000)
                                    let! tasks = Server.getSchedule()
                                    vList.Value <- tasks
                                } |> Async.Start
                            )
                            yield tr [
                                td [ text time ]
                                td [ text mission ]
                                td [ cancel ]
                            ] :> Doc
                    ]
                ]
            )
        let runningTaskView =
            vRunning.View
            |> Doc.BindView (fun tasks ->
                tableAttr [ attr.``class`` "table" ] [
                    thead [
                        tr [
                            th [ text "Instance" ]
                            th [ text "Event"]
                            th []
                        ]
                    ]
                    tbody [
                        for (instance, desc) in tasks do
                            yield tr [
                                td [ text instance ]
                                td [ text desc ]
                            ] :> Doc
                    ]
                ]
            )
        let statusView =
            vStatus.View
            |> View.Map (fun msg ->
                match msg with
                | "" -> Doc.Empty
                | msg -> divAttr [ attr.``class`` "alert alert-danger" ; Attr.Create "role" "alert" ] [ text msg ] :> Doc
            )

        async {
            let! tasks = Server.getSchedule()
            vList.Value <- tasks
            let! running = Server.getRunning()
            vRunning.Value <- running
        }
        |> Async.Start

        let instances = List.ofArray instances
        div [
            form [
                divAttr [ attr.``class`` "form-group row"] [
                    divAttr [ attr.``class`` "col-sm-1" ] []
                    labelAttr [ attr.``for`` "idInstance" ; attr.``class`` "col-sm-1 form-control-label" ] [ text "Instance" ]
                    divAttr [ attr.``class`` "col-sm-8" ] [ Doc.Select [ attr.``type`` "text" ; attr.``class`` "form-control" ; attr.id "idInstance" ; attr.placeholder "[[[yyyy/]mm]/dd] hh:mm" ] id instances vInstance ]
                    divAttr [ attr.``class`` "col-sm-2" ] []
                ]
                divAttr [ attr.``class`` "form-group row" ] [
                    divAttr [ attr.``class`` "col-sm-1" ] []
                    labelAttr [ attr.``for`` "idTime" ; attr.``class`` "col-sm-1 form-control-label" ] [ text "Time" ]
                    divAttr [ attr.``class`` "col-sm-8" ] [ Doc.Input [ attr.``type`` "text" ; attr.``class`` "form-control" ; attr.id "idTime" ; attr.placeholder "[[[yyyy/]mm]/dd] hh:mm" ] vTime ]
                    divAttr [ attr.``class`` "col-sm-2" ] [ getServerTime ]
                ]
                divAttr [ attr.``class`` "form-group row" ] [
                    Doc.Radio [ attr.``class`` "col-sm-1" ] ScheduledEvent.LoadMission vChoice
                    labelAttr [ attr.``for`` "idMission" ; attr.``class`` "col-sm-1 form-control-label" ] [ text "Mission" ]
                    divAttr [ attr.``class`` "col-sm-8" ] [ Doc.Input [ attr.``type`` "text" ; attr.``class`` "form-control" ; attr.id "idMission" ; attr.placeholder @"sds\mission.sds" ] vMission ]
                    divAttr [ attr.``class`` "col-sm-2" ] []
                ]
                divAttr [ attr.``class`` "form-group row" ] [
                    Doc.Radio [ attr.``class`` "col-sm-1" ] ScheduledEvent.Shutdown vChoice
                    labelAttr [ attr.``for`` "idReason" ; attr.``class`` "col-sm-1 form-control-label" ] [ text "Shutdown" ]
                    divAttr [ attr.``class`` "col-sm-8" ] [ Doc.Input [ attr.``type`` "text" ; attr.``class`` "form-control" ; attr.id "idReason" ; attr.placeholder "Message" ] vReason ]
                    divAttr [ attr.``class`` "col-sm-2" ] []
                ]
                divAttr [ attr.``class`` "form-group row" ] [
                    Doc.Radio [ attr.``class`` "col-sm-1" ] ScheduledEvent.Broadcast vChoice
                    labelAttr [ attr.``for`` "idBroadcast" ; attr.``class`` "col-sm-1 form-control-label" ] [ text "Broadcast" ]
                    divAttr [ attr.``class`` "col-sm-8" ] [ Doc.Input [ attr.``type`` "text" ; attr.``class`` "form-control" ; attr.id "idBroadcast" ; attr.placeholder "Message" ] vBroadcast ]
                    divAttr [ attr.``class`` "col-sm-2" ] []
                ]
                divAttr [ attr.``class`` "form-group row" ] [
                    Doc.Radio [ attr.``class`` "col-sm-1" ] ScheduledEvent.StartCampaign vChoice
                    labelAttr [ attr.``for`` "idCampaign" ; attr.``class`` "col-sm-1 form-control-label" ] [ text "Campaign" ]
                    divAttr [ attr.``class`` "col-sm-8" ] []
                    divAttr [ attr.``class`` "col-sm-2" ] []
                ]
                divAttr [ attr.``class`` "form-group row" ] [
                    Doc.Radio [ attr.``class`` "col-sm-1" ] ScheduledEvent.ResetCampaign vChoice
                    labelAttr [ attr.``for`` "idCampaignReset" ; attr.``class`` "col-sm-1 form-control-label" ] [ text "Campaign reset" ]
                    divAttr [ attr.``class`` "col-sm-8" ] [ Doc.Input [ attr.``type`` "text" ; attr.``class`` "form-control" ; attr.id "idScenario" ; attr.placeholder "MoscowWinter" ] vScenario ]
                    divAttr [ attr.``class`` "col-sm-2" ] []
                ]
                divAttr [ attr.``class`` "form-group row" ] [
                    divAttr [ attr.``class`` "col-sm-offset-1 col-sm-11" ] [ submit ]
                ]
                divAttr [ attr.``class`` "form-group row" ] [
                    divAttr [ attr.``class`` "col-sm-offset-1 col-sm-11" ] [ restart ]
                ]
            ]
            Doc.EmbedView statusView
            taskView
            runningTaskView
            getList
        ]

    let playerLoginForm() =
        let vPlayerName = Var.Create ""
        let vPassword = Var.Create ""
        let vLoginPressed = Var.Create 0
        let credentials =
            View.Map2 (fun x y -> (x, y)) vPlayerName.View vPassword.View
        let confirmed =
            credentials
            |> View.MapAsync (fun (login, pwd) ->
                async {
                    let! loggedIn = Server.loginPlayer(login, pwd)
                    if loggedIn then
                        return "Success", Some login
                    else
                        return "Failed", None
                }
            )
            |> View.SnapshotOn ("", None) vLoginPressed.View
        let status =
            confirmed
            |> View.Map fst
            |> View.Map (function
                | "Success" as txt -> pAttr [ attr.``class`` "bg-success" ] [ text txt ]
                | txt -> pAttr [ attr.``class`` "bg-danger" ] [ text txt ])
        let confirmedPilot =
            confirmed
            |> View.Map snd
        confirmedPilot,
        div [
            formRow [
                formCell 4 [ Doc.Input [ attr.``class`` "form-control" ; attr.id "idPlayer" ; attr.placeholder "Pilot" ] vPlayerName ]
                formCell 4 [ Doc.Input [ attr.``type`` "password" ; attr.``class`` "form-control" ; attr.id "idPassword" ; attr.placeholder "Password" ] vPassword ]
                formCell 4 [ Doc.Button "Login" [ attr.``class`` "btn btn-primary" ] (fun () -> vLoginPressed.Value <- vLoginPressed.Value + 1) ]
            ]
            formRow [
                Doc.EmbedView status
            ]
        ] :> Doc

    let playerLowerPage(instance : string, pilotName : View<string option>) =
        let vPlaneGift = Var.Create ""
        let vGiftRecipient = Var.Create ""
        let vAirfieldGift = Var.Create ""

        let planes =
            pilotName
            |> View.MapAsync (fun pilot ->
                async {
                    match pilot with
                    | None -> return Map.empty
                    | Some pilot -> return! Server.getReservedPlanes(instance, pilot)
                }
            )

        let spawns =
            pilotName
            |> View.MapAsync (fun pilot ->
                async {
                    match pilot with
                    | None -> return Map.empty
                    | Some pilot -> return! Server.getFreshSpawns(instance, pilot)
                }
            )

        let cash =
            pilotName
            |> View.MapAsync (fun pilot ->
                async {
                    match pilot with
                    | None -> return Map.empty
                    | Some pilot -> return! Server.getCashReserve(instance, pilot)
                }
            )

        let cashDoc =
            cash
            |> View.Map (fun cashByCoalition ->
                cashByCoalition
                |> Map.toSeq
                |> Seq.map (fun (coalition, cash) -> sprintf "Cash (%s): %0.0f" coalition cash)
                |> String.concat ", "
                )
            |> Doc.TextView

        let planesDoc =
            View.Map2 (fun reserved (spawns : Map<string, Map<string, float32>>) ->
                tableAttr [ attr.``class`` "table" ] [
                    thead [
                        tr [
                            th [ text "Airfield" ]
                            th [ text "Planes(number or health)"]
                        ]
                    ]
                    tbody [
                        for coalition, spawns in spawns |> Map.toSeq do
                            yield tr [
                                td [text <| sprintf "Fresh spawns at %s rear airfield" coalition]
                                td [
                                    spawns
                                    |> Map.toSeq
                                    |> Seq.sortBy fst
                                    |> Seq.map (fun (plane, qty) -> sprintf "%s (%0.1f)" plane qty)
                                    |> String.concat ", "
                                    |> text]
                            ] :> Doc
                        for airfield, planes in reserved |> Map.toSeq |> Seq.sortBy fst do
                            yield tr [
                                td [text airfield]
                                td [
                                    planes
                                    |> Map.toSeq
                                    |> Seq.sortBy fst
                                    |> Seq.map (fun (plane, qty) -> sprintf "%s (%0.1f)" plane qty)
                                    |> String.concat ", "
                                    |> text]
                            ] :> Doc
                    ]
                ]
            ) planes spawns
            |> Doc.EmbedView

        let airfieldNames =
            View.Do {
                let! reserved = planes
                let names = reserved |> Map.toSeq |> Seq.map fst |> List.ofSeq
                return
                    "" :: names
            }

        let validPlaneNames =
            View.Do {
                let! airfield = vAirfieldGift.View
                let! reserved = planes
                return
                    reserved
                    |> Map.tryFind airfield
                    |> Option.map (fun m -> m |> Map.toSeq |> Seq.map fst |> List.ofSeq)
                    |> function None -> [""] | Some x -> "" :: x
            }

        let descrDoc =
            Doc.Concat [
                airfieldNames
                |> View.Map (fun names -> "Airfields with planes: " + String.concat "," names)
                |> Doc.TextView
            ]

        let giftDoc =
            let vGiftStatus = Var.Create None
            div [
                yield formRow [
                    yield formCell 3 [ Doc.SelectDyn [ attr.``type`` "text"; attr.``class`` "form-control"; attr.placeholder "Airfield" ] id airfieldNames vAirfieldGift ] :> Doc
                    yield formCell 3 [ Doc.SelectDyn [ attr.``type`` "text"; attr.``class`` "form-control"; attr.placeholder "Plane" ] id validPlaneNames vPlaneGift ] :> Doc
                    yield formCell 3 [ Doc.Input [ attr.``type`` "text"; attr.``class`` "form-control"; attr.placeholder "Recipient" ] vGiftRecipient ] :> Doc
                    let data =
                        View.Do {
                            let! airfield = vAirfieldGift.View
                            let! plane = vPlaneGift.View
                            let! pilotName = pilotName
                            return airfield, plane, pilotName
                        }
                    let recipient =
                        vGiftRecipient.View
                        |> View.MapAsync (fun (name : string) ->
                            async {
                                match name with
                                | "all" ->
                                    return Choice1Of2 None
                                | name ->
                                    let! found = Server.searchUserName name
                                    return
                                        match found with
                                        | Some _ -> Choice1Of2 found
                                        | None -> Choice2Of2 "Recipient unknown"
                            })
                        |> View.Map2 (fun (airfield, plane, giverName) recipient ->
                            match giverName, recipient with
                            | None, _ -> Choice2Of2 "Giver unknown"
                            | Some giverName, Choice1Of2 recipient -> Choice1Of2 (giverName, recipient, airfield, plane)
                            | _, Choice2Of2 msg -> Choice2Of2 msg
                            ) data
                    yield divAttr [ attr.``class`` "col-sm-3" ] [
                        Doc.ButtonView "Give" [ attr.``class`` "btn" ] recipient (fun donationStatus ->
                            Async.Start (
                                async {
                                    match donationStatus with
                                    | Choice1Of2(giver, recipient, airfield, plane) ->
                                        let! status = Server.giftPlane(instance, giver, recipient, airfield, plane)
                                        vGiftStatus.Value <- Some status
                                    | Choice2Of2 msg ->
                                        vGiftStatus.Value <- Some(Choice2Of2 msg)
                                }))
                        ] :> Doc
                ] :> Doc
                let statusView =
                    vGiftStatus.View
                    |> View.Map (function
                        | None -> Doc.Empty
                        | Some (Choice1Of2 msg) -> pAttr [ attr.``class`` "bg-success" ] [ text msg ] :> Doc
                        | Some (Choice2Of2 msg) -> pAttr [ attr.``class`` "bg-danger" ] [ text msg ] :> Doc
                    )
                yield Doc.EmbedView statusView
            ]

        Doc.Concat [
            cashDoc
            planesDoc
            giftDoc
            //descrDoc
        ]

    let PlayerPage(instance : string) =
        async {
            let! isAdmin = Server.isLoggedAsAdmin()
            let! player = Server.loggedInAsPlayer()
            match isAdmin, player with
            | true, _ ->
                let vPilotNameIn = Var.Create ""
                let vPilotNameOut = Var.Create None
                return Doc.Concat [
                    formAttr [ attr.``class`` "form-inline" ]  [
                        labelAttr [ attr.``for`` "idPlayer" ; attr.``class`` "form-control-label" ] [ text "Player" ]
                        Doc.Input [ attr.``class`` "form-control" ; attr.id "idPlayer" ; attr.placeholder "Pilot" ] vPilotNameIn
                        Doc.Button "Show" [] (fun () -> vPilotNameOut.Value <- Some vPilotNameIn.Value)
                    ] :> Doc
                    playerLowerPage(instance, vPilotNameOut.View)
                ]
            | false, Some _ ->
                return playerLowerPage(instance, View.Const player)
            | false, None ->
                let pilotNameOut, loginForm = playerLoginForm()
                return Doc.Concat [
                    loginForm
                    playerLowerPage(instance, pilotNameOut)
                ]
        }
        |> Doc.Async
