
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server Compact Edition
-- --------------------------------------------------
-- Date Created: 05/11/2018 15:50:09
-- Generated from EDMX file: C:\Users\johann\Documents\sturmovikservercontrol\SturmovikServerControl\Model1.edmx
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- NOTE: if the constraint does not exist, an ignorable error will be reported.
-- --------------------------------------------------

    ALTER TABLE [Bans] DROP CONSTRAINT [FK__Bans__0000000000000033];
GO
    ALTER TABLE [NamingSet] DROP CONSTRAINT [FK_PlayerGuidsNaming];
GO
    ALTER TABLE [NamingSet] DROP CONSTRAINT [FK_PlayerNamesNaming];
GO
    ALTER TABLE [PinCodesSet] DROP CONSTRAINT [FK_PlayerGuidsPinCodes];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- NOTE: if the table does not exist, an ignorable error will be reported.
-- --------------------------------------------------

    DROP TABLE [Bans];
GO
    DROP TABLE [PlayerGuids];
GO
    DROP TABLE [PlayerNames];
GO
    DROP TABLE [DbVersion];
GO
    DROP TABLE [NamingSet];
GO
    DROP TABLE [PinCodesSet];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Bans'
CREATE TABLE [Bans] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [PlayerGuid] int  NOT NULL,
    [Expiration] datetime  NOT NULL,
    [Comment] nvarchar(4000)  NOT NULL
);
GO

-- Creating table 'PlayerGuids'
CREATE TABLE [PlayerGuids] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [PlayerId] uniqueidentifier  NOT NULL,
    [ProfileId] uniqueidentifier  NOT NULL
);
GO

-- Creating table 'PlayerNames'
CREATE TABLE [PlayerNames] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(128)  NOT NULL
);
GO

-- Creating table 'DbVersion'
CREATE TABLE [DbVersion] (
    [Version] int  NOT NULL
);
GO

-- Creating table 'NamingSet'
CREATE TABLE [NamingSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Date] datetime  NOT NULL,
    [PlayerGuidsId] int  NOT NULL,
    [PlayerNamesId] int  NOT NULL,
    [DateTicks] bigint  NOT NULL
);
GO

-- Creating table 'PinCodesSet'
CREATE TABLE [PinCodesSet] (
    [PlayerGuidsId] int  NOT NULL,
    [PinCode] nvarchar(4000)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Bans'
ALTER TABLE [Bans]
ADD CONSTRAINT [PK_Bans]
    PRIMARY KEY ([Id] );
GO

-- Creating primary key on [Id] in table 'PlayerGuids'
ALTER TABLE [PlayerGuids]
ADD CONSTRAINT [PK_PlayerGuids]
    PRIMARY KEY ([Id] );
GO

-- Creating primary key on [Id] in table 'PlayerNames'
ALTER TABLE [PlayerNames]
ADD CONSTRAINT [PK_PlayerNames]
    PRIMARY KEY ([Id] );
GO

-- Creating primary key on [Version] in table 'DbVersion'
ALTER TABLE [DbVersion]
ADD CONSTRAINT [PK_DbVersion]
    PRIMARY KEY ([Version] );
GO

-- Creating primary key on [Id] in table 'NamingSet'
ALTER TABLE [NamingSet]
ADD CONSTRAINT [PK_NamingSet]
    PRIMARY KEY ([Id] );
GO

-- Creating primary key on [PlayerGuidsId] in table 'PinCodesSet'
ALTER TABLE [PinCodesSet]
ADD CONSTRAINT [PK_PinCodesSet]
    PRIMARY KEY ([PlayerGuidsId] );
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [PlayerGuid] in table 'Bans'
ALTER TABLE [Bans]
ADD CONSTRAINT [FK__Bans__0000000000000033]
    FOREIGN KEY ([PlayerGuid])
    REFERENCES [PlayerGuids]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__Bans__0000000000000033'
CREATE INDEX [IX_FK__Bans__0000000000000033]
ON [Bans]
    ([PlayerGuid]);
GO

-- Creating foreign key on [PlayerGuidsId] in table 'NamingSet'
ALTER TABLE [NamingSet]
ADD CONSTRAINT [FK_PlayerGuidsNaming]
    FOREIGN KEY ([PlayerGuidsId])
    REFERENCES [PlayerGuids]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PlayerGuidsNaming'
CREATE INDEX [IX_FK_PlayerGuidsNaming]
ON [NamingSet]
    ([PlayerGuidsId]);
GO

-- Creating foreign key on [PlayerNamesId] in table 'NamingSet'
ALTER TABLE [NamingSet]
ADD CONSTRAINT [FK_PlayerNamesNaming]
    FOREIGN KEY ([PlayerNamesId])
    REFERENCES [PlayerNames]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PlayerNamesNaming'
CREATE INDEX [IX_FK_PlayerNamesNaming]
ON [NamingSet]
    ([PlayerNamesId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------