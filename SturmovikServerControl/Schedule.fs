﻿// SturmovikServerControl Copyright (C) 2016 Johann Deneux <johann.deneux@gmail.com>
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Schedule

open System
open System.IO
open MBrace.FsPickler

type ScheduledAction =
    | OpenSDS of filename: string
    | Shutdown of reason: string
    | Broadcast of message: string
    | CampaignInit
    | CampaignReset of scenario: string
    | Step of Async<(DateTime * ScheduledAction * string) list>

let rec wrapApiScheduledTask (t : Async<CampaignServerControl.Api.ScheduledTask>) =
    async {
        let! next = t
        match next with
        | CampaignServerControl.Api.NoTask ->
            return []
        | CampaignServerControl.Api.SomeTask(time, f, description) ->
            return [time, wrapApiScheduledTask f, description]
    }
    |> Step

let convertApiScheduledTask (t : CampaignServerControl.Api.ScheduledTask) =
    match t with
    | CampaignServerControl.Api.NoTask ->
        async {
            return []
        }
    | CampaignServerControl.Api.SomeTask(time, f, description) ->
        async {
            return [time, wrapApiScheduledTask f, description]
        }
    |> Step

type MessageToRunner =
    | PostAction of System.DateTime * ScheduledAction * string
    | Cancel of int

type Scheduler(instances : string list, executeAction) =
    let serializer = FsPickler.CreateXmlSerializer()
    let logger = NLog.LogManager.GetCurrentClassLogger()

    // Persistence of schedule
    let saveFileName instance =
        let path = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location)
        Path.Combine(path, sprintf "scheduled-%s.xml" instance)

    let saveActions instance scheduledActions =
        let tempFile = Path.GetTempFileName()
        try
            do
                use file = File.OpenWrite tempFile
                // Remove Step entries, don't try to serialize Asyncs
                let filtered =
                    scheduledActions
                    |> List.filter (function (_, _, Step _, _) -> false | _ -> true)
                serializer.Serialize(file, filtered)
            File.Copy(tempFile, saveFileName instance, true)
        finally
            File.Delete(tempFile)

    let loadActions instance =
        use file = File.OpenRead (saveFileName instance)
        serializer.Deserialize(file) : (int * DateTime * ScheduledAction * string) list
        |> List.mapi (fun idx (_, t, a, desc) -> idx, t, a, desc) // Rebuild indices

    // Each runner maintains its own queue as it works, but there is also a copy used to inform clients of the queues' contents.
    let queues =
        instances
        |> List.map (fun instance -> instance, (obj(), ref []))
        |> dict

    // Lock the queue of an instance, and set the list of actions.
    let updateQueue instance (actions : (int * DateTime * ScheduledAction * string) list) =
        match queues.TryGetValue instance with
        | false, _ ->
            ()
        | true, (l, stored) ->
            lock l (fun () -> stored := actions)

    // Lock the queue of an instance, and get its list of actions.
    let getQueue instance =
        match queues.TryGetValue instance with
        | false, _ ->
            []
        | true, (l, stored) ->
            lock l (fun () -> stored.Value)

    // Currently running actions, used for GUI only
    let currentlyRunning =
        instances
        |> List.map (fun instance -> instance, (obj(), ref[]))
        |> dict

    let updateRunning instance (actions : _ list) =
        match currentlyRunning.TryGetValue instance with
        | false, _ ->
            ()
        | true, (l, stored) ->
            lock l (fun () -> stored := actions)

    let getRunning instance =
        match currentlyRunning.TryGetValue instance with
        | false, _ ->
            []
        | true, (l, stored) ->
            lock l (fun () -> stored.Value)

    // Execute all scheduled actions that have become ready
    let rec processScheduled ticket instance executeAction (now : System.DateTime) items =
        async {
            match items with
            | [] -> return ticket, []
            | (idx : int, scheduledAt, action, _) as item :: rest ->
                if scheduledAt < now then
                    updateQueue instance rest
                    updateRunning instance [item]
                    let! newActions = executeAction instance action
                    let newActions =
                        newActions
                        |> List.mapi (fun i (t, action, desc) -> (ticket + i, t, action, desc))
                    return! processScheduled (ticket + List.length newActions) instance executeAction now (rest @ newActions)
                else
                    let! ticket, tail = processScheduled ticket instance executeAction now rest
                    let actions = item :: tail 
                    updateQueue instance actions
                    updateRunning instance []
                    return ticket, actions
        }

    let period = 5000

    // One runner per instance
    let runners =
        instances
        |> List.map (fun instance ->
            instance,
            fun (mb : MailboxProcessor<MessageToRunner>) ->
                let rec work ticket actions : Async<unit> =
                    async {
                        // Run actions that are ready to run, if any
                        let! ticket, actions = processScheduled ticket instance executeAction DateTime.UtcNow actions
                        try
                            saveActions instance actions
                        with
                        | e -> logger.Error((sprintf "Scheduler.runner %s" instance) + " " + (sprintf "Failed to save actions: %s" e.Message))
                        // Add new actions posted through the mailbox
                        let! action = mb.TryReceive(period)
                        match action with
                        | Some(PostAction(t, action, desc)) ->
                            // Add at the correct position, according to scheduled time
                            let rec insert ((_, t, _, _) as action) xs =
                                match xs with
                                | (_, ty, _, _) as y :: ys when ty <= t -> y :: insert action ys
                                | _ -> action :: xs
                            // Keep going
                            let actions = insert (ticket, t, action, desc) actions
                            return! work (ticket + 1) actions
                        | Some(Cancel idx) ->
                            let actions =
                                actions
                                |> List.filter (fun(idx2, _, _, _) -> idx <> idx2)
                            updateQueue instance actions
                            return! work ticket actions
                        | None ->
                            return! work ticket actions
                    }
                async {
                    let actions =
                        try
                            loadActions instance
                        with
                        | e ->
                            logger.Error((sprintf "Scheduler.runner %s" instance) + " " + (sprintf "Failed to load actions: %s" e.Message))
                            []
                    return! work (List.length actions) actions
                }
            |> MailboxProcessor.Start)
        |> dict

    member this.TryPost(instance, scheduled, action, desc) =
        match PartialDateTime.parseDate scheduled with
        | Some scheduled ->
            let dateTime = PartialDateTime.resolveDate scheduled
            match runners.TryGetValue instance with
            | true, runner ->
                runner.Post(PostAction(dateTime, action, desc))
                true
            | false, _ ->
                logger.Error("Scheduler.TryPost " + (sprintf "No runner for instance '%s'" instance))
                false
        | None ->
            logger.Info("Scheduler.TryPost " + (sprintf "Failed to parse scheduled = %s" scheduled))
            false

    member this.Actions =
        [
            for instance in instances do
                yield!
                    getQueue instance
                    |> List.map (fun (idx, t, _, desc) -> instance, idx, t, desc)
        ]
        |> List.sortBy (fun (instance, idx, t, _) -> instance, t, idx)

    member this.Running =
        [
            for instance in instances do
                yield!
                    getRunning instance
                    |> List.map (fun (_, _, _, desc) -> instance, desc)
        ]
        |> List.sortBy (fun (instance, _) -> instance)

    member this.Cancel(instance, idx) =
        match runners.TryGetValue instance with
        | true, runner ->
            runner.Post(Cancel idx)
        | false, _ ->
            logger.Error("Scheduler.Cancel " + (sprintf "No runner for instance '%s'" instance))
