﻿module SturmovikServerControl.PlannerIntegration

open CampaignServerControl.Api
open WebSharper.Core.Attributes
open System.Numerics

type LatLng =
    { lat : float
      lng : float }

type Point =
    { latLng : LatLng
      name : string
      [<Name "type">] Type : string
      color : string
      notes : string
    }

type Route =
    { latLngs : LatLng[]
      name : string
      speed : int
      speeds : int[]
    }

type Root =
    { mapHash : string
      frontline : float[][][][] // tuple, lines, sides, group
      routes : Route[]
      points : Point[]
    }


type MapGraphics.MapName with
    member this.ToPlannerJson =
        match this with
        | MapGraphics.Moscow ->
            "#moscow"
        | MapGraphics.Kuban ->
            "#kuban"
        | MapGraphics.Stalingrad ->
            "#stalingrad"
        | MapGraphics.VelikieLuki ->
            "#luki"


type System.Numerics.Vector2 with
    member this.ToPlannerJson(map : MapGraphics.MapName) =
        let bounds =
            let moscowSize = 281000.0f
            match map with
            | MapGraphics.Moscow -> 0.0f, 0.0f, moscowSize, moscowSize
            | MapGraphics.Stalingrad -> 0.0f, 0.0f, 230400.0f, 358400.0f
            | MapGraphics.VelikieLuki -> 0.0f, 0.0f, 102400.0f, 166400.0f
            | MapGraphics.Kuban -> 35400.0f, 35200.0f, 323000.0f, 451000.0f
        let outSize =
            match map with
            | MapGraphics.Kuban -> 100.0f, 144.0f
            | MapGraphics.Stalingrad -> 160.0f, 256.0f
            | MapGraphics.Moscow -> 192.0f, 192.0f
            | MapGraphics.VelikieLuki -> 160.0f, 256.0f
        let transform(latitude, longitude) =
            let h0, l0, h, l = bounds
            let sh, sl = outSize
            sh * (latitude - h0) / (h - h0),
            sl * (longitude - l0) / (l - l0)
        let lat, lng = transform(this.X, this.Y)
        { lat = float lat
          lng = float lng
        }


type MapGraphics.MapIconSymbol with
    member this.ToPlannerType =
        match this with
        | MapGraphics.Tank -> "taw-tank"
        | MapGraphics.Truck -> "taw-supply"
        | MapGraphics.Factory -> "taw-depo"
        | MapGraphics.House -> "taw-def"
        | MapGraphics.Clash -> "ground-combat"
        | MapGraphics.Base -> "re-point"
        | MapGraphics.Airfield -> "taw-af"
        | _ -> ""


type MapGraphics.TeamColor with
    member this.ToPlannerColor =
        match this with
        | MapGraphics.Red -> "red"
        | MapGraphics.Gray -> "black"


type MapGraphics.MapIcon with
    member this.ToPlannerJson(map) =
        { latLng = this.Position.ToPlannerJson(map)
          name = this.Label |> Option.defaultValue ""
          Type = this.Icon.ToPlannerType
          color = this.Color.ToPlannerColor
          notes = this.Description |> Option.defaultValue "" }

type MapGraphics.MapHoverNote with
    member this.ToPlannerJson(map) =
        { latLng = this.Position.ToPlannerJson(map)
          name = ""
          Type = ""
          color = "black"
          notes = this.Text }

type MapGraphics.MapArea with
    member this.ToPlannerJson(map) =
        let vertices =
            [|
                for v in this.Boundaries do
                    let latLng = v.ToPlannerJson(map)
                    yield [| latLng.lat; latLng.lng |]
            |]
        if this.Color = Vector3(1.0f, 0.0f, 0.0f) then
            [|[||]; vertices|]
        else
            [|vertices; [||]|]

type MapGraphics.MapPackage with
    member this.ToPlannerJson =
        let points1 =
            this.Icons
            |> Seq.map (fun icon -> icon.ToPlannerJson(this.Name))
            |> Array.ofSeq
        let points2 =
            this.HoverNotes
            |> Seq.map (fun note -> note.ToPlannerJson(this.Name))
            |> Array.ofSeq
        let frontlines =
            this.Areas
            |> Seq.map (fun a -> a.ToPlannerJson(this.Name))
            |> Array.ofSeq
        { mapHash = this.Name.ToPlannerJson
          frontline = frontlines
          routes = [||]
          points = Array.concat [points1; points2]
        }