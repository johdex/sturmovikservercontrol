﻿/// Database of players
module SturmovikServerControl.Database

open System
open System.IO
open FSharp.Data.TypeProviders
open System.Data.Linq.SqlClient
open System.Data.SqlServerCe

let private logger = NLog.LogManager.GetCurrentClassLogger()

let tryGuid (s : string) =
    try
        Guid(s)
        |> Some
    with _ -> None

[<Literal>]
let private edmxConn = @"metadata=res://*/Model1.csdl|res://*/Model1.ssdl|res://*/Model1.msl;provider=System.Data.SqlServerCe.4.0;provider connection string=';Data Source=sscontrol.sdf';"

let private sqlCeConn = "Data Source = sscontrol.sdf"

type private edmx = EdmxFile<"Model1.edmx", ResolutionFolder = __SOURCE_DIRECTORY__>

let private getCtx() = new edmx.Model.Entities(edmxConn)

[<Literal>]
let private dbName = "sscontrol.sdf"

let createDb() =
    File.Delete(dbName)
    use ctx = getCtx()
    ctx.CreateDatabase()
    let numChanges = ctx.SaveChanges()
    let version = ctx.DbVersion.CreateObject()
    version.Version <- 2
    ctx.AddToDbVersion(version)
    let numChanges2 = ctx.SaveChanges()
    ()

let private dbUpgrades : (int * int * (Data.Common.DbConnection -> bool)) list =
    let runTransaction (db : Data.Common.DbConnection) cmds =
        db.Open()
        let tr = db.BeginTransaction()
        let cmd = db.CreateCommand()
        cmd.Transaction <- tr
        try
            try
                for txt in cmds do
                    cmd.CommandText <- txt
                    cmd.ExecuteNonQuery() |> ignore
                tr.Commit()
                true
            with
            | _ ->
                tr.Rollback()
                false
        finally
            tr.Dispose()
            cmd.Dispose()
            db.Close()
        
    [
        (0, 1, fun db ->
            let cmds =
                [
                    "CREATE TABLE PinCodesSet (Id int IDENTITY(1,1) NOT NULL, PinCode nvarchar(4000)  NOT NULL, PlayerGuids_Id int  NOT NULL)"
                    "ALTER TABLE PinCodesSet ADD CONSTRAINT PK_PinCodesSet PRIMARY KEY (Id)"
                    "ALTER TABLE PinCodesSet ADD CONSTRAINT FK_PlayerGuidsPinCodes FOREIGN KEY (PlayerGuids_Id) REFERENCES PlayerGuids(Id) ON DELETE NO ACTION ON UPDATE NO ACTION"
                    "CREATE INDEX IX_FK_PlayerGuidsPinCodes ON PinCodesSet (PlayerGuids_Id)"
                ]
            runTransaction db cmds)
        (1, 2, fun db ->
            let cmds =
                [
                    "ALTER TABLE [PinCodesSet] DROP CONSTRAINT [FK_PlayerGuidsPinCodes]"
                    "DROP TABLE [PinCodesSet]"
                    "CREATE TABLE [PinCodesSet] ([PlayerGuidsId] int  NOT NULL, [PinCode] nvarchar(4000)  NOT NULL)"
                    "ALTER TABLE [PinCodesSet] ADD CONSTRAINT [PK_PinCodesSet]PRIMARY KEY ([PlayerGuidsId] )"
                ]
            runTransaction db cmds)
    ]

let upgradeDb() =
    use ctx = getCtx()
    let version =
        query {
            for v in ctx.DbVersion do
                select v.Version
        } |> Seq.max
    let rec work v =
        let candidates =
            dbUpgrades
            |> List.filter (fun (from_, _, _) -> from_ = v)
        match candidates with
        | [] -> ()
        | _ :: _ ->
            let _, to_, f = candidates |> List.maxBy (fun (_, x, _) -> x)
            let backupName = sprintf "sscontrol-v%d-%d.sdf" v to_
            try
                File.Copy("sscontrol.sdf", backupName)
                let success =
                    use db = new SqlCeConnection(sqlCeConn) in f db
                if success then
                    logger.Info (sprintf "Updated database from %d to %d" v to_)
                    let version = ctx.DbVersion.CreateObject()
                    version.Version <- to_
                    ctx.SaveChanges() |> ignore
                    work to_
                else
                    logger.Error (sprintf "Failed to update database from %d to %d" v to_)
            with
            | exc ->
                logger.Error (sprintf "Exception while trying to update database: %s" exc.Message)
                raise(Exception("Failed to update database", exc))
    work version

let openDb() =
    if not(File.Exists(dbName)) then
        createDb()
    upgradeDb()

type PlayerData =
    { Name : string
      ProfileId : Guid
      PlayerId : Guid }

let updateNames (names : PlayerData list) =
    let now = DateTime.UtcNow
    use ctx = getCtx()
    for name in names do
        let playerId = name.PlayerId
        // Get or create player GUIDs
        let guids =
            query {
                for guids in ctx.PlayerGuids do
                    where (guids.PlayerId = playerId)
                    select guids
                    exactlyOneOrDefault
            }
        let guids =
            match guids with
            | null ->
                let newGuids = ctx.PlayerGuids.CreateObject()
                newGuids.PlayerId <- playerId
                newGuids.ProfileId <- name.ProfileId
                ctx.AddToPlayerGuids(newGuids)
                newGuids
            | x -> x
        // Get or create visible name
        let pname =
            query {
                for pname in ctx.PlayerNames do
                    where (pname.Name = name.Name)
                    select pname
                    exactlyOneOrDefault
            }
            |>  function
                | null ->
                    let newName = ctx.PlayerNames.CreateObject()
                    newName.Name <- name.Name
                    ctx.AddToPlayerNames(newName)
                    newName
                | x -> x
        ctx.SaveChanges() |> ignore
        // Get or create name association
        let naming =
            query {
                for naming in ctx.NamingSet do
                    where (naming.PlayerNames.Name = name.Name)
                    where (naming.PlayerGuids.PlayerId = playerId)
            }
            |> Seq.tryHead
            |>  function
                | None ->
                    let newNaming = ctx.NamingSet.CreateObject()
                    newNaming.Date <- now
                    newNaming.DateTicks <- now.Ticks
                    newNaming.PlayerGuidsId <- guids.Id
                    newNaming.PlayerNamesId <- pname.Id
                    ctx.AddToNamingSet(newNaming)
                    newNaming
                | Some oldNaming ->
                    oldNaming // No change, nothing to update
        ()
    ctx.SaveChanges() |> ignore

let getBannedPlayerIds (now : System.DateTime) =
    let ticks = now.Ticks
    use ctx = getCtx()
    query {
        for ban in ctx.Bans do
            where (ban.Expiration > now)
            select ban.PlayerGuids.PlayerId
    }
    |> Seq.map string
    |> Set.ofSeq

let banUser(playerId : string, release : System.DateTime, comment : string) =
    match tryGuid playerId with
    | Some playerId ->
        use ctx = getCtx()
        let guids =
            query {
                for guid in ctx.PlayerGuids do
                    where (guid.PlayerId = playerId)
                    select guid
                    exactlyOneOrDefault
            }
        match guids with
        | null ->
            ()
        | guids ->
            let ban = ctx.Bans.CreateObject()
            ban.PlayerGuid <- guids.Id
            ban.Expiration <- release
            ban.Comment <- comment
            ctx.AddToBans(ban)
        ctx.SaveChanges() |> ignore
    | None ->
        ()

let searchUserByPlayerId (playerId : string) =
    match tryGuid playerId with
    | Some playerId ->
        use ctx = getCtx()
        let res =
            query {
                for guid in ctx.PlayerGuids do
                join naming in ctx.NamingSet on (guid.Id = naming.PlayerGuidsId)
                join name in ctx.PlayerNames on (naming.PlayerNamesId = name.Id)
                where (guid.PlayerId = playerId)
                sortByDescending naming.DateTicks
                select (naming.Date, name.Name, guid.PlayerId)
            }
            |> Seq.truncate 10
            |> List.ofSeq
        res
    | None ->
        []

let searchUserByName (pattern : string) =
    use ctx = getCtx()
    let res =
        query {
            for guid in ctx.PlayerGuids do
            join naming in ctx.NamingSet on (guid.Id = naming.PlayerGuidsId)
            join name in ctx.PlayerNames on (naming.PlayerNamesId = name.Id) 
            where (name.Name.Contains(pattern))
            sortByDescending naming.DateTicks
            select (naming.Date, name.Name, guid.PlayerId)
        }
        |> Seq.truncate 10
        |> List.ofSeq
    res

let tryGetGuidByName (pattern : string) =
    use ctx = getCtx()
    let res =
        query {
            for guid in ctx.PlayerGuids do
            join naming in ctx.NamingSet on (guid.Id = naming.PlayerGuidsId)
            join name in ctx.PlayerNames on (naming.PlayerNamesId = name.Id) 
            where (name.Name = pattern)
            sortByDescending naming.DateTicks
            select guid.PlayerId
        }
        |> Seq.tryHead
    res

let unbanUserByPlayerId (playerId : string) =
    match tryGuid playerId with
    | Some playerId ->
        use ctx = getCtx()
        query {
            for guid in ctx.PlayerGuids do
            join ban in ctx.Bans on (guid.Id = ban.PlayerGuid)
            where (guid.PlayerId = playerId)
            select ban
        }
        |> Seq.iter (fun ban -> ctx.Bans.DeleteObject(ban))
        ctx.SaveChanges() |> ignore
    | None ->
        ()

let private tryGetUserByGuidQ (ctx : edmx.Model.Entities) (user : Guid) =
    query {
        for guid in ctx.PlayerGuids do
        where (guid.PlayerId = user)
        select guid
    }

let tryGetUserPinCode (userId : Guid) =
    use ctx = getCtx()
    let guid = tryGetUserByGuidQ ctx userId |> Seq.tryHead
    match guid with
    | Some guid ->
        let pin =
            query {
                for pin in ctx.PinCodesSet do
                where (pin.PlayerGuidsId = guid.Id)
                select pin.PinCode
            }
            |> Seq.tryHead
        match pin with
        | Some _ -> pin
        | None ->
            let symbols = "123456789ABCDEFGHJKLMNPQRSTUVWXYZ"
            let pinCode =
                let random = System.Random()
                Array.init 6 (fun _ -> let idx = random.Next(symbols.Length) in string symbols.[idx])
                |> String.concat ""
            let pin = ctx.PinCodesSet.CreateObject()
            pin.PlayerGuidsId <- guid.Id
            pin.PinCode <- pinCode
            ctx.AddToPinCodesSet(pin)
            ctx.SaveChanges() |> ignore
            Some pinCode
    | None ->
        None