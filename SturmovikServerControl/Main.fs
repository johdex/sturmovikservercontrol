// SturmovikServerControl Copyright (C) 2016 Johann Deneux <johann.deneux@gmail.com>
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

namespace SturmovikServerControl

open Util

open WebSharper
open WebSharper.Sitelets
open WebSharper.UI.Next
open WebSharper.UI.Next.Server
open System.IO

type EndPoint =
    | [<EndPoint "/">] Home
    | [<EndPoint "/status">] Status
    | [<EndPoint "/login">] Login
    | [<EndPoint "/schedule">] Schedule
    | [<EndPoint "/campaign">] Campaign of instance: string
    | [<EndPoint "/about">] About
    | [<EndPoint "/player">] PlayerHome of instance: string
    | [<EndPoint "/data">] [<Wildcard>] Data of instance: string * dataKind: string * args: string list

module Templating =
    open WebSharper.UI.Next.Html

    type MainTemplate = Templating.Template<"Main.html">

    // Compute a menubar where the menu item for the given endpoint is active
    let MenuBar (ctx: Context<EndPoint>) endpoint : Doc list =
        let ( => ) txt act =
             liAttr [if endpoint = act then yield attr.``class`` "active"] [
                aAttr [attr.href (ctx.Link act)] [text txt]
             ]
        [
            yield li ["Login" => EndPoint.Login] :> Doc
            yield li ["Status" => EndPoint.Status] :> Doc
            yield li ["Schedule" => EndPoint.Schedule] :> Doc
            yield li ["Players' site" => EndPoint.Home] :> Doc
            yield li ["About" => EndPoint.About] :> Doc
        ]

    let Main ctx action title body =
        Content.Page(
            MainTemplate.Doc(
                title = title,
                menubar = MenuBar ctx action,
                body = body
            )
        )

    let PlayerMenuBar instances (ctx: Context<EndPoint>) endpoint : Doc list =
        let ( => ) txt act =
             liAttr [if endpoint = act then yield attr.``class`` "active"] [
                aAttr [attr.href (ctx.Link act)] [text txt]
             ]
        let adminLink = ctx.Link EndPoint.Schedule
        [
            for instance in instances do
                yield li [instance => EndPoint.PlayerHome(instance)] :> Doc
                yield li [client <@ Client.AdminLink (adminLink, "Admin") @>] :> Doc
        ]

    let PlayerMain instances ctx action title body =
        Content.Page(
            MainTemplate.Doc(
                title = title,
                menubar = PlayerMenuBar instances ctx action,
                body = body
            )
        )

module Site =
    open WebSharper.UI.Next.Html
    open CampaignSupport
    open PlannerIntegration

    let StatusPage instances ctx =
        Templating.Main ctx EndPoint.Home "Status" [
            h1 [ text "Server status" ]
            div [client <@ Client.Status(instances) @>]
        ]

    let LoginPage ctx =
        Templating.Main ctx EndPoint.Login "Log in" [
            h1 [ text "Log in" ]
            div [client <@ Client.Login() @>]
        ]

    let SchedulePage instances ctx =
        Templating.Main ctx EndPoint.Schedule "Schedule" [
            h1 [ text "Scheduled missions" ]
            div [ client <@ Client.Schedule(instances) @> ]
        ]

    let AboutPage ctx =
        Templating.Main ctx EndPoint.About "About" [
            h1 [text "About"]
            p [text <| sprintf "Sturmovik Server Control version %s" AssemblyInfo.Constants.version ]
            p [text "This site monitors and controls an instance of IL-2 Sturmovik's dedicated server."]
        ]

    let CampaignMap (instance : string) =
        match ServerState.plugins.TryGetValue(instance) with
        | true, (_, _, gfx) ->
            Content.Json (gfx.MapPackage.ToPlannerJson)
        | false, _ ->
            Content.Json (MapGraphics.DefaultMapPackage.ToPlannerJson)

    let PlayerHomePage instances (instance : string) ctx =
        Templating.PlayerMain instances ctx (EndPoint.PlayerHome(instance)) "Home" [
            h1 [text (sprintf "Pilot status on %s" instance)] :> Doc
            div [ client <@ Client.PlayerPage(instance) @> ] :> Doc
        ]

    let Main instances =
        Application.MultiPage (fun ctx endpoint ->
            match endpoint with
            | EndPoint.Home ->
                let instance =
                    match instances with
                    | [||] -> ""
                    | _ -> instances.[0] 
                PlayerHomePage instances instance ctx
            | EndPoint.Status -> StatusPage instances ctx
            | EndPoint.Login -> LoginPage ctx
            | EndPoint.Schedule -> SchedulePage instances ctx
            | EndPoint.Campaign(instance) -> CampaignMap instance
            | EndPoint.About -> AboutPage ctx
            | EndPoint.PlayerHome(instance) -> PlayerHomePage instances instance ctx
            | EndPoint.Data(instance, dataKind, args) ->
                async {
                    let! data = ServerState.requestData(instance, dataKind, args)
                    match data with
                    | Ok json ->
                        return! Content.Custom(
                            Status = Http.Status.Ok,
                            Headers = [Http.Header.Custom "Content-Type" "application/json"],
                            WriteBody = fun stream ->
                                use w = new System.IO.StreamWriter(stream)
                                w.Write(json)
                        )
                    | Error _ ->
                        return! Content.Json(data)
                }
        )

module Entry =
    // Logging
    let private logger = NLog.LogManager.GetCurrentClassLogger()
    let info src m = logger.Info(sprintf "%s: %s" src m)
    let verbose src m = logger.Debug(sprintf "%s: %s" src m)
    let error src m = logger.Error(sprintf "%s: %s" src m)

    exception WarpStartFailureException of string

    open System.Web.UI
    open System.Threading
    open System

    let mkNetShAdvice url =
        [
            "Failed to bind http listener."
            "Maybe executing the following command (without leading $) as administrator can resolve this."
            sprintf "$ netsh http add urlacl url=%s user=%s\\%s" url System.Environment.UserDomainName System.Environment.UserName
        ]
        |> String.concat "\n"

    [<EntryPoint>]
    [<assembly:WebResource("Image1.png", "image/png")>]
    [<assembly:WebResource("EventIcons0.png", "image/png")>]
    [<assembly:WebResource("EventIcons1.png", "image/png")>]
    [<assembly:WebResource("EventIcons2.png", "image/png")>]
    [<assembly:WebResource("EventIcons3.png", "image/png")>]
    [<assembly:WebResource("truck-red.png", "image/png")>]
    [<assembly:WebResource("truck-gray.png", "image/png")>]
    [<assembly:WebResource("tank-red.png", "image/png")>]
    [<assembly:WebResource("tank-gray.png", "image/png")>]
    [<assembly:WebResource("plane-red.png", "image/png")>]
    [<assembly:WebResource("plane-gray.png", "image/png")>]
    [<assembly:WebResource("ship-red.png", "image/png")>]
    [<assembly:WebResource("ship-gray.png", "image/png")>]
    [<assembly:WebResource("artillery-red.png", "image/png")>]
    [<assembly:WebResource("artillery-gray.png", "image/png")>]
    [<assembly:WebResource("house-red.png", "image/png")>]
    [<assembly:WebResource("house-gray.png", "image/png")>]
    [<assembly:WebResource("factory-red.png", "image/png")>]
    [<assembly:WebResource("factory-gray.png", "image/png")>]
    [<assembly:WebResource("clash.png", "image/png")>]
    [<assembly:WebResource("death-red.png", "image/png")>]
    [<assembly:WebResource("death-gray.png", "image/png")>]
    [<assembly:WebResource("exclaim.png", "image/png")>]
    [<assembly:WebResource("parachute-red.png", "image/png")>]
    [<assembly:WebResource("parachute-gray.png", "image/png")>]
    [<assembly:WebResource("sun.png", "image/png")>]
    [<assembly:WebResource("moon.png", "image/png")>]
    [<assembly:WebResource("cloud.png", "image/png")>]
    [<assembly:WebResource("rain.png", "image/png")>]
    do
        let exitStatus =
#if DEBUG
            NLog.LogManager.Configuration.AddRuleForOneLevel(NLog.LogLevel.Debug, "CON")
            NLog.LogManager.ReconfigExistingLoggers()
#endif
            try
                let config = Configuration.configuration()
                SturmovikServerControl.Database.openDb()
                let urls =
                    config.ControllerUrls
                    |> List.map string
                match config.Validate() with
                | [] ->
                    let instances = config.Instances |> List.map (fun instance -> instance.Name)
                    match Array.ofSeq instances with
                    | [||] ->
                        error "main" "No DServer instance configured."
                        1
                    | instances ->
                        // Cancelation token source used to terminate web server
                        use terminate = new CancellationTokenSource()
                        // Event used to request termination
                        let terminationEvent = Event<unit>()
                        use __unsubscribe = terminationEvent.Publish.Subscribe(fun () -> terminate.Cancel())
                        ServerState.setTerminationEvent(terminationEvent)
                        try
                            // Start web server
                            let warp = Warp.Run(Site.Main instances, urls = urls)
                            info "main" <| sprintf "Listening on %s" (String.concat ", " urls)
                            terminationEvent.Publish.Add(fun () -> warp.Stop())
                        with
                        | exc ->
                            let msg =
                                urls
                                |> List.map mkNetShAdvice
                                |> String.concat "\n"
                            raise(WarpStartFailureException(msg))
                        // Start ban loop
                        let banCheckLoop =
                            async {
                                while true do
                                    do! Server.checkBans()
                                    do! Async.Sleep 60000
                            }
                        Async.Start(banCheckLoop, terminate.Token)
                        if config.Instances |> Seq.exists (fun config -> config.ChatLogUpdateEnabled && config.ChatLogDirectory.IsSome) then
                            // Start cutchatlog loop
                            let cutChatLog =
                                async {
                                    while true do
                                    do! Async.Sleep 15000
                                    do! ServerState.cutAllChatLogs()
                                }
                            Async.Start(cutChatLog, terminate.Token)
                        info "main" "Ban checker started"
                        // Keep running
                        let wait = async {
                            while true do
                                do! Async.Sleep(System.Int32.MaxValue)
                        }
                        try
                            Async.RunSynchronously(wait, cancellationToken = terminate.Token)
                        with
                        | :? OperationCanceledException -> ()
                        // Call restart.bat, which contains a sleep command followed by an invocation of this program
                        try
                            use restart = new System.Diagnostics.Process()
                            restart.StartInfo.UseShellExecute <- true
                            restart.StartInfo.FileName <- "restart.bat"
                            restart.Start() |> ignore
                            info "main" "will restart in 60s"
                        with
                        | _ ->
                            info "main" "Failed to schedule restart"
                        0

                | errors ->
                    for e in errors do
                        error "main" e
                    1
            with
            | :? Configuration.ConfigurationLoadException as exc ->
                error "main" <| sprintf "%s" exc.Message
                1
            | WarpStartFailureException(msg) ->
                error "main" <| sprintf "%s" msg
                1
        System.Environment.Exit(exitStatus)