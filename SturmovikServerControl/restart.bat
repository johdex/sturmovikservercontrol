SEVZIP="C:\Program Files\7-Zip\7z.exe"

timeout /t 60

rem Update if drop file exists
if exist "%SEVZIP%" (
    if exist drop.zip (
        %SEVZIP% x drop.zip SturmovikServerControl/bin/Release -odrop
        xcopy /C /Y drop\SturmovikServerControl\bin\Release\* .
        rmdir /Q /S drop
        del drop.zip
    )
)

SturmovikServerControl.exe
