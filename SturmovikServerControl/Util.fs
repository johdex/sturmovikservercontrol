﻿module Util

let (|EmptyString|NonEmptyString|) (s : string) =
    if System.String.IsNullOrEmpty(s) then
        EmptyString
    else
        NonEmptyString s

let stringToOpt (s : string) =
    match s with
    | null
    | EmptyString -> None
    | NonEmptyString s -> Some s