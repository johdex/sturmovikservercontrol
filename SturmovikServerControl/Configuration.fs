﻿// SturmovikServerControl Copyright (C) 2016 Johann Deneux <johann.deneux@gmail.com>
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Configuration

open FSharp.Configuration
open System.IO
open Util

// Type of data extracted from configuration files
type ConfigurationFile = YamlConfig<"SampleConfig.yaml">

let internal newConfiguration() =
    let config = ConfigurationFile()
    let instance = ConfigurationFile.Server_Type.Instances_Item_Type()

    config.Server.AdminPasswordHash <- ""
    config.Server.ControllerUrls <- ResizeArray<_> [System.Uri "http://localhost:8083/"]
    config.Server.Instances <- ResizeArray<_>[instance]

    instance.Name <- "Primary"
    instance.Description <- "No description yet."
    instance.ServerHostname <- "localhost"
    instance.ServerPort <- 8991
    instance.Login <- "rconlogin"
    instance.Password <- "rconpassword"
    instance.OffLoadServerInput <- "ReqKill"
    instance.GameDirectory <- @"C:\IL-2 Sturmovik Battle of Stalingrad"
    instance.LogDirectory <- ""
    instance.CampaignPluginAssembly <- ""
    instance.CampaignPluginType <- ""
    instance.CampaignPluginConfig <- ""
    config


type CampaignPlugin =
    { Assembly : string
      TypeName : string
      Config : string
    }

// Type of data used in the rest of the code. Mirrors ConfigurationFile, with optional strings
// modelled as string options.
type InstanceConfiguration =
    {  Name : string
       Description : string
       ServerHostname : string
       ServerPort : int
       Login : string
       Password : string
       OffLoadServerInput : string option
       GameDirectory : string
       LogDirectory : string option
       ChatLogDirectory : string option
       ChatLogUpdateEnabled : bool
       CampaignPlugin : CampaignPlugin option
    }
with
    static member internal FromFile(instance : ConfigurationFile.Server_Type.Instances_Item_Type) =
        let plugin =
            match stringToOpt instance.CampaignPluginAssembly, stringToOpt instance.CampaignPluginType, stringToOpt instance.CampaignPluginConfig with
            | Some asm, Some typ, Some config ->
                Some {
                    Assembly = asm
                    TypeName = typ
                    Config = config
                }
            | _ ->
                None
        {  Name = instance.Name
           Description = instance.Description
           ServerHostname = instance.ServerHostname
           ServerPort = instance.ServerPort
           Login = instance.Login
           Password = instance.Password
           OffLoadServerInput = stringToOpt instance.OffLoadServerInput
           GameDirectory = instance.GameDirectory
           LogDirectory = stringToOpt instance.LogDirectory
           ChatLogDirectory = stringToOpt instance.ChatLogDirectory
           ChatLogUpdateEnabled = instance.ChatLogUpdateEnabled
           CampaignPlugin = plugin
        }

    member this.Validate() =
        let isValidIdentifier (s : string) =
            let re = System.Text.RegularExpressions.Regex("^[a-zA-Z][a-zA-Z0-9_-]*$")
            re.IsMatch(s, 0)
        [
            if System.String.IsNullOrWhiteSpace(this.Name) then
                yield "Name of instance cannot be empty"
            else
                if not (isValidIdentifier this.Name) then
                    yield sprintf "Name of instance '%s' is not a valid identifier" this.Name
                let validateOptDir desc optDir =
                    match optDir with
                    | Some dir when not(System.IO.Directory.Exists(dir)) ->
                        sprintf "%s '%s' of instance '%s' does not exist" desc dir this.Name
                        |> Some
                    | _ ->
                        None

                if System.String.IsNullOrWhiteSpace(this.ServerHostname) then
                    yield sprintf "ServerHostname of instance '%s' cannot be empty" this.Name
                if this.ServerPort <= 0 || this.ServerPort > 65535 then
                    yield sprintf "ServerPort of instance '%s' is out of range" this.Name
                if System.String.IsNullOrWhiteSpace(this.Login) then
                    yield sprintf "Login of instance '%s' cannot be empty" this.Name
                if System.String.IsNullOrWhiteSpace(this.Password) then
                    yield sprintf "Password of instance '%s' cannot be empty" this.Name
                if System.String.IsNullOrWhiteSpace(this.GameDirectory) then
                    yield sprintf "GameDirectory of instance '%s' cannot be empty" this.Name
                elif not(System.IO.Directory.Exists(this.GameDirectory)) then
                    yield sprintf "GameDirectory of instance '%s' '%s' does not exist" this.Name this.GameDirectory
                match validateOptDir "LogDirectory" this.LogDirectory with
                | Some x -> yield x
                | None -> ()
                match validateOptDir "ChatLogDirectory" this.ChatLogDirectory with
                | Some x -> yield x
                | None -> ()
        ]

type Configuration =
    {  AdminPasswordHash : string option
       ControllerUrls : System.Uri list
       Instances : InstanceConfiguration list
    }
with
    static member internal FromFile(config : ConfigurationFile) =
        let instances =
            config.Server.Instances
            |> Seq.map InstanceConfiguration.FromFile
            |> List.ofSeq
        {  AdminPasswordHash = stringToOpt config.Server.AdminPasswordHash
           ControllerUrls = List.ofSeq config.Server.ControllerUrls
           Instances = instances
        }

    member this.Validate() =
        [
            if this.ControllerUrls.IsEmpty then
                yield "ControllerUrls cannot be empty"
            for instance in this.Instances do
                yield! instance.Validate()
            let dupInstanceNames =
                this.Instances
                |> Seq.groupBy (fun instance -> instance.Name)
                |> Seq.filter (fun (name, instances) -> Seq.length instances > 1)
                |> Seq.map fst
            for name in dupInstanceNames do
                yield sprintf "Multiple instances share the same name '%s'" name
        ]

    member this.CheckAdminPassword(password : string) =
        match this.AdminPasswordHash with
        | None
        | Some "" -> false
        | Some hash ->
            let hashBytes = System.Convert.FromBase64String(hash)
            let salt = hashBytes.[0..15]
            let hashed = Array.skip 16 hashBytes
            use pbkdf2 = new System.Security.Cryptography.Rfc2898DeriveBytes(password, salt, 10000)
            let hashed2 = pbkdf2.GetBytes(hashed.Length)
            hashed = hashed2


let loadConfig(filename : string) =
    let config = ConfigurationFile()
    // Clear default values from the sample file
    config.Server.ControllerUrls.Clear()
    // If we clear the default server instances,
    // we get an exception about ServerType+ItemType not being comparable and therefore cannot be included into a list.
    // That makes no sense to me, but apparently the default constructor of ConfigurationFile creates an empty server instance list anyway, so there's no need to clear it.
//    config.Server.Instances.Clear()
    config.Server.AdminPasswordHash <- ""
    // Load the actual values
    config.Load(filename)
    // Transfer to config data structure
    Configuration.FromFile config


type ConfigurationLoadException(filename : string, inner : System.Exception) =
    inherit System.Exception(sprintf "Failed to load configuration file '%s' because '%s'" filename inner.Message, inner)


let configuration() =
    let exeLocation =
        System.Reflection.Assembly.GetExecutingAssembly().Location
        |> System.IO.Path.GetDirectoryName
    let configFilename = System.IO.Path.Combine(exeLocation, "config.yaml")
    try
        loadConfig(configFilename)
    with
    | e ->
        raise(ConfigurationLoadException(configFilename, e))