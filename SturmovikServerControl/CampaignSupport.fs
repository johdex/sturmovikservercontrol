﻿// SturmovikServerControl Copyright (C) 2017 Johann Deneux <johann.deneux@gmail.com>
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

module SturmovikServerControl.CampaignSupport

open Util
open System
open System.IO
open System.Diagnostics
open CampaignServerControl.Api

type PlayerId(playerData : RConClient.PlayerData) =
    member x.Data = playerData

    interface CampaignServerControl.Api.PlayerId with
        member x.GetName() = playerData.Name

type TeamId(name, teamId) =
    member x.TeamId = teamId

    interface CampaignServerControl.Api.TeamId with
        member x.GetName() = name

type Logging(instance) =
    let logger = NLog.LogManager.GetCurrentClassLogger()
    interface CampaignServerControl.Api.LoggingApi with
        member x.LogError(m) = logger.Error(sprintf "From %s: %s" instance m)
        member x.LogInfo(m) = logger.Info(sprintf "From %s: %s" instance m)

type MapGraphics() =
    let mutable mapPackage : MapGraphics.MapPackage = 
        { Name = MapGraphics.Stalingrad
          AnchoredReports = []
          HoverNotes = []
          SideNotes = []
          Areas = []
          Icons = []
        }

    interface ServerRenderingApi with
        member this.SetPackage(pkg) =
            mapPackage <- pkg

    member this.MapPackage = mapPackage

    static member DefaultMapPackage : MapGraphics.MapPackage =
        { Name = MapGraphics.Stalingrad
          AnchoredReports = []
          HoverNotes = []
          SideNotes = []
          Areas = []
          Icons = []
        }

type GameServer(queue : RConClient.ClientMessageQueue, banUserFunc : string * int * string -> Async<unit>) =
    let alliesTeam = TeamId("Allies", 1)
    let axisTeam = TeamId("Axis", 2)
    let logger = NLog.LogManager.GetCurrentClassLogger()

    interface CampaignServerControl.Api.ServerControlApi with
        member x.GetAxisTeam() = upcast axisTeam
        member x.GetAlliesTeam() = upcast alliesTeam
        member x.GetPlayerList =
            logger.Trace("GetPlayerList")
            async {
                let! players = queue.Run(lazy queue.Client.GetPlayerList())
                match players with
                | Some(Some data) ->
                    return
                        data
                        |> Array.map(fun d -> PlayerId(d) :> CampaignServerControl.Api.PlayerId)
                        |> List.ofArray
                | _ ->
                    return []
            }
        member x.MessageAll(messages) =
            logger.Trace("MessageAll " + string(messages))
            async {
                for m in messages do
                    let! x = queue.Run(
                                lazy async {
                                    let! status = queue.Client.MessageAll m
                                    logger.Trace(sprintf "MessageAll Sent '%s', status '%s'" m status)
                                    return()
                                })
                    if Option.isNone x then
                        logger.Error("MessageAll Failed")
                    do! Async.Sleep(500)
            }
        member x.MessagePlayer(player, messages) =
            logger.Trace("MessagePlayer " + string(player) + ": " + string(messages))
            async {
                let clientId =
                    match player with
                    | :? PlayerId as x -> x.Data.ClientId
                    | _ -> failwith "Unsupported implementation of PlayerId"
                for m in messages do
                    let! x = queue.Run(
                                lazy async {
                                    let! status = queue.Client.MessagePlayer(clientId, m)
                                    logger.Trace(sprintf "MessagePlayer Sent '%s', status '%s'" m status)
                                    return()
                                })
                    if Option.isNone x then
                        logger.Error("MessagePlayer Failed")
                    do! Async.Sleep(500)
            }
        member x.MessageTeam(team, messages) =
            logger.Trace("MessageTeam " + string(team) + ": " + string(messages))
            async {
                let teamId =
                    match team with
                    | :? TeamId as x -> x.TeamId
                    | _ -> failwith "Unsupported implementation of TeamId"
                for m in messages do
                    let! x = queue.Run(
                                lazy async {
                                    let! status = queue.Client.MessageTeam(teamId, m)
                                    logger.Trace(sprintf "MessageTeam Sent '%s', status '%s'" m status)
                                    return()
                                })
                    if Option.isNone x then
                        logger.Error("MessageTeam Failed")
                    do! Async.Sleep(500)
            }
        member x.SkipMission =
            logger.Trace("SkipMission")
            async {
                let! x = queue.Run(lazy queue.Client.ServerInput "ReqMissionEnd")
                if Option.isNone x then
                    logger.Error("SkipMission Failed")
                return()
            }

        member x.ServerInput(name) =
            logger.Debug("ServerInput: " + name)
            async {
                let! x = queue.Run(lazy queue.Client.ServerInput name)
                if Option.isNone x then
                    logger.Error("ServerInput Failed")
                return()
            }

        member x.BanPlayer(player, hours) =
            logger.Trace("Ban player " + string(player) + " for " + string(hours) + " hours")
            async {
                
                let clientId, nickId, name =
                    match player with
                    | :? PlayerId as player ->
                        player.Data.ClientId, player.Data.PlayerId, player.Data.Name
                    | _ ->
                        failwith "Unknown implementation of PlayerId"
                let! x = queue.Run(lazy queue.Client.BanPlayer(clientId))
                if Option.isNone x then
                    logger.Error("BanPlayer Failed")
                do! banUserFunc(nickId, hours, name)
            }

        member x.KickPlayer(player) =
            logger.Trace("Kick player " + string(player))
            async {
                let clientId =
                    match player with
                    | :? PlayerId as player ->
                        player.Data.ClientId
                    | _ ->
                        failwith "Unknown implementation of PlayerId"
                let! x = queue.Run(lazy queue.Client.KickPlayer(clientId))
                if Option.isNone x then
                    logger.Error("GameServer.KickPlayer Failed")
            }
        
        member x.TryGetPlayerPin(playerId) =
            logger.Trace("Get pin " + string(playerId))
            async {
                return SturmovikServerControl.Database.tryGetUserPinCode(playerId)
            }

        member x.TryGetPlayerByUserId(playerId) =
            let playerId = string playerId
            logger.Trace("Get player by guid " + playerId)
            async {
                let! players = queue.Run(lazy queue.Client.GetPlayerList())
                match players with
                | Some(Some players) ->
                    return
                        players
                        |> Seq.tryFind (fun p -> p.PlayerId = playerId)
                        |> Option.map (fun d -> upcast PlayerId(d))
                | _ ->
                    return None
            }