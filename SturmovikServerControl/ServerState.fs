﻿// SturmovikServerControl Copyright (C) 2016, 2018 Johann Deneux <johann.deneux@gmail.com>
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

module SturmovikServerControl.ServerState


open System
open System.IO
open System.Diagnostics
open System.Reflection
open MBrace.FsPickler

let private logger = NLog.LogManager.GetCurrentClassLogger()

let config = Configuration.configuration()

let mutable private terminationEvent = Event<unit>()

let setTerminationEvent x = terminationEvent <- x

let requestTermination() =
    logger.Info("Server termination requested")
    terminationEvent.Trigger()

let queues =
    config.Instances
    |> Seq.map (fun instance ->
        instance.Name,
        new RConClient.ClientMessageQueue(instance.ServerHostname, instance.ServerPort, instance.Login, instance.Password))
    |> dict

let instances =
    config.Instances
    |> Seq.map (fun instance -> instance.Name, instance)
    |> dict

let plugins =
    config.Instances
    |> Seq.choose (fun instance ->
        match instance.CampaignPlugin with
        | Some pluginSpec ->
            let supportApis =
                match queues.TryGetValue instance.Name with
                | true, queue ->
                    let gfx = CampaignSupport.MapGraphics()
                    let banUser(user, duration, comment) =
                        async {
                            let release = DateTime.UtcNow.AddHours(float duration)
                            Database.banUser(user, release, comment)
                        }
                    let apis =
                        { CampaignServerControl.Api.Logging = CampaignSupport.Logging(instance.Name)
                          CampaignServerControl.Api.ServerControl = CampaignSupport.GameServer(queue, banUser)
                          CampaignServerControl.Api.MapGraphics = gfx
                        }
                    Some(apis, gfx)
                | false, _ ->
                    logger.Error(sprintf "No RConClient available for %s" instance.Name)
                    None
            match supportApis with
            | Some (support, gfx) ->
#if DEACTIVATED_TEST_CODE
                let phony =
                    { new CampaignServerControl.Api.CampaignServerApi with
                          member this.GetData(arg1, arg2) =
                            async {
                                return Error "No data to return"
                            }
                          member this.GetPlayerCashReserve(playerName) =
                            async {
                                return
                                    match playerName with
                                    | "tanguy" -> 1234.0f
                                    | _ -> 0.0f
                            }
                          member this.GetPlayerFreshSpawns(playerName) =
                            async {
                                return
                                    match playerName with
                                    | "tanguy" -> [ "Fighter", 2.0f ] |> Map.ofList
                                    | _ -> Map.empty
                            }
                          member this.GetPlayerReservedPlanes(playerName) =
                            async {
                                return
                                    match playerName with
                                    | "tanguy" -> Map.ofList [ "Sorcelles", Map.ofList [ "Mirage", 1.0f ] ]
                                    | _ -> Map.empty
                            }
                          member this.GiftReservedPlane(giverName, recipientName, planeName, airfieldName) =
                            async {
                                return Error "Gifting not implemented"
                            }
                          member this.Init(arg1) = ()
                          member this.Reset(config, scenario) = Choice1Of2(CampaignServerControl.Api.ScheduledTask.NoTask)
                          member this.StartOrResume(config) = Choice1Of2(CampaignServerControl.Api.ScheduledTask.NoTask)
                    }
                Some (instance.Name, (phony, "config.yaml", gfx))
#else
                try
                    let asm = Assembly.LoadFrom(pluginSpec.Assembly)
                    let typ = asm.GetType(pluginSpec.TypeName)
                    let cstr = typ.GetConstructor([||])
                    let pluginObject = cstr.Invoke([||]) :?> CampaignServerControl.Api.CampaignServerApi
                    pluginObject.Init(support)
                    Some(instance.Name, (pluginObject, pluginSpec.Config, gfx))
                with
                | e ->
                    logger.Error(sprintf "Failed to load type '%s' from assembly '%s': '%s'" pluginSpec.TypeName pluginSpec.Assembly e.Message)
                    None
#endif
            | None ->
                None
        | None ->
            None)
    |> dict

let scheduler =
    Schedule.Scheduler(
        config.Instances |> List.map (fun x -> x.Name),
        fun instance request ->
            match request with
            | Schedule.OpenSDS(filename) ->
                async {
                    match instances.TryGetValue(instance), queues.TryGetValue(instance) with
                    | (true, instance), (true, queue) ->
                        let startDir =
                            Path.Combine(instance.GameDirectory, "bin", "game")
                            |> Path.GetFullPath
                        let procs =
                            Process.GetProcessesByName("DServer")
                            |> Array.filter (fun proc -> Path.GetFullPath(Path.GetDirectoryName(proc.MainModule.FileName)) = startDir)
                        // Kill all DServers started from that instance directory (should be one or zero).
                        if procs.Length = 0 then
                            logger.Info "No DServer running, none to kill."
                        elif procs.Length > 1 then
                            logger.Warn (sprintf "Multiple DServer instances running under %s" instance.GameDirectory)
                        for running in procs do
                            logger.Info "Killing DServer process..."
                            try
                                running.Kill()
                            with
                            | e ->
                                logger.Error (sprintf "Failed to kill DServer.exe: %s" e.Message)
                        // Start DServer with given SDS file.
                        try
                            let dataPath = Path.GetFullPath(Path.Combine(instance.GameDirectory, "data"))
                            let exePath = Path.Combine(instance.GameDirectory, "bin", "game", "DServer.exe")
                            let filename =
                                Path.Combine("..", "..", "data", filename)
                            logger.Debug (sprintf "exePath = %s, filename = %s" exePath filename)
                            let si = ProcessStartInfo(exePath, filename)
                            si.WorkingDirectory <- Path.GetDirectoryName(exePath)
                            si.UseShellExecute <- false
                            let proc = Process.Start(si)
                            logger.Info (sprintf "%s [%d] started." proc.ProcessName proc.Id)
                            proc.Dispose()
                            return []
                        with
                        | e ->
                            logger.Error (sprintf "Failed to start DServer.exe: %s" e.Message)
                            return []
                    | _, (false, _)
                    | (false, _), _ ->
                        logger.Error (sprintf "No such server instance %s" instance)
                        return []
                }
            | Schedule.Shutdown(reason) ->
                async {
                    match queues.TryGetValue(instance) with
                    | true, queue ->
                        let! response = queue.Run(lazy queue.Client.MessageAll(reason))
                        logger.Trace("Shutdown reason broadcast response " + (string response))
                        do! Async.Sleep(60000)
                        let! response = queue.Run(lazy queue.Client.Shutdown())
                        logger.Debug("Shutdown response " + (string response))
                        return []
                    | false, _ ->
                        logger.Error (sprintf "No such server instance %s to shut down" instance)
                        return []
                }
            | Schedule.Broadcast(message) ->
                async {
                    match queues.TryGetValue(instance) with
                    | true, queue ->
                        let! response = queue.Run(lazy queue.Client.MessageAll(message))
                        logger.Trace("Shutdown reason broadcast response" + (string response))
                        return []
                    | false, _ ->
                        logger.Error(sprintf "No such server instance %s to broadcast to" instance)
                        return []
                }
            | Schedule.CampaignReset scenario ->
                async {
                    match plugins.TryGetValue(instance) with
                    | true, (plugin, config, gfx) ->
                        try
                            match plugin.Reset(config, scenario) with
                            | Choice1Of2 x ->
                                let steps = Schedule.convertApiScheduledTask(x)
                                return [DateTime.UtcNow, steps, "reset campaign state"]
                            | Choice2Of2 reason ->
                                logger.Error (sprintf "Reset: Campaign plugin returned error '%s'" reason)
                                return []
                        with
                        | e ->
                            logger.Error (sprintf "Campaign reset failed: '%s'" e.Message)
                            return []
                    | false, _ ->
                        logger.Error (sprintf "No plugin initialized for server instance '%s' to reset campaign" instance)
                        return []
                }
            | Schedule.CampaignInit ->
                async {
                    match plugins.TryGetValue(instance) with
                    | true, (plugin, config, gfx) ->
                        try
                            match plugin.StartOrResume config with
                            | Choice1Of2 x ->
                                let steps = Schedule.convertApiScheduledTask(x)
                                return [DateTime.UtcNow, steps, "run campaign"]
                            | Choice2Of2 reason ->
                                logger.Error (sprintf "Campaign plugin returned error '%s' during init" reason)
                                return []
                        with
                        | e ->
                            logger.Error (sprintf "Campaign init failed: '%s'" e.Message)
                            return []
                    | false, _ ->
                        logger.Error (sprintf "No plugin initialized for server instance '%s' to run init" instance)
                        return []
                }
            | Schedule.Step(conts) ->
                async {
                    try
                        return! conts
                    with
                    | e ->
                        logger.Error (sprintf "Failed to execute step: '%s'" e.Message)
                        return []
                }
    )

let isAdmin(user) =
    match user with
    | Some "admin" -> true
    | _ -> false

/// Force creation of a new chatlog, and concat the old files to avoid generating too many small files
let cutChatLog(instance) =
    async {
        match instances.TryGetValue(instance), queues.TryGetValue(instance) with
        | (true, config), (true, queue) ->
            match config.ChatLogDirectory with
            | Some chatlogs when config.ChatLogUpdateEnabled ->
                let cutlogs =
                    Directory.EnumerateFiles(chatlogs, "*.chatlog")
                    |> Array.ofSeq
                    |> Array.sortBy File.GetCreationTimeUtc
                use outFile = File.AppendText(Path.Combine(config.GameDirectory, "data", "chatlogs", "collected-chatlogs.txt"))
                for file in cutlogs.[0 .. cutlogs.Length - 24] do // 24: Let the files for the latest 6 minutes live, to make sure the campaign plugin gets to see them.
                    try
                        for line in File.ReadAllLines(file) do
                            outFile.WriteLine(line)
                        File.Delete(file)
                    with
                    | _ -> ()
                let! _ = queue.Run(lazy queue.Client.CutChatLog())
                ()
            | Some _
            | None ->
                ()
        | _ ->
            ()
    }

let cutAllChatLogs() =
    async {
        for instance in instances.Keys do
            do! cutChatLog(instance)
    }

let requestData(instance, dataKind, args) =
    async {
        match plugins.TryGetValue(instance) with
        | false, _ ->
            return Error (sprintf "Instance '%s' not found" instance)
        | true, (plugin, _, _) ->
            return! plugin.GetData(dataKind, args)
    }