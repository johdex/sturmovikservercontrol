﻿// SturmovikServerControl Copyright (C) 2016 Johann Deneux <johann.deneux@gmail.com>
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

module PartialDateTime

type MonthSpec =
    { MonthOfYear : int
      Year : int option
    }
type DateSpec =
    { DayOfMonth : int
      Month : MonthSpec option
    }
type TimeSpec =
    { Hour : int
      Minute : int
      Date : DateSpec option
    }
        
let parseDate s : TimeSpec option =
    let parseInt (s : string) =
        let s = s.TrimStart()
        let regex = System.Text.RegularExpressions.Regex("[0-9]+")
        let m = regex.Match(s)
        if m.Success then
            let n = System.Int32.Parse(m.Value)
            Some (n, s.Substring(m.Value.Length))
        else
            None

    let skipDateSeparator s =
        if String.length s > 0 then
            if s.[0] = '/' then
                Some (s.Substring(1))
            else
                None
        else
            None

    let skipHourSeparator s =
        if String.length s > 0 then
            if s.[0] = ':' then
                Some (s.Substring(1))
            else
                None
        else
            None

    let parseMinute prev s =
        match parseInt s with
        | Some (n, s) ->
            match (s.Trim()), prev with
            | _, [] -> None // mandatory hour missing
            | "", hour :: rest ->
                match rest with
                | [] -> Some { Hour = hour ; Minute = n ; Date = None }
                | day :: rest ->
                    match rest with
                    | [] -> Some { Hour = hour ; Minute = n ; Date = Some { DayOfMonth = day ; Month = None }}
                    | month :: rest ->
                        match rest with
                        | [] -> Some { Hour = hour ; Minute = n ; Date = Some { DayOfMonth = day ; Month = Some { MonthOfYear = month ; Year = None }}}
                        | [year] -> Some { Hour = hour ; Minute = n ; Date = Some { DayOfMonth = day ; Month = Some { MonthOfYear = month ; Year = Some year }}}
                        | _ -> failwith "Unreachable case"
            | junk, _ -> None
        | None -> None

    let parseHour prev s =
        match parseInt s with
        | Some(n, s) ->
            match skipHourSeparator s with
            | Some s -> parseMinute (n :: prev) s
            | None -> None
        | None -> None

    let parseDayOrHour prev s =
        match parseInt s with
        | Some(n, s) ->
            match skipHourSeparator s with
            | Some s -> parseMinute (n :: prev) s
            | None -> parseHour (n :: prev) s
        | None -> None

    let parseMonthOrDayOrHour prev s =
        match parseInt s with
        | Some(n, s) ->
            match skipDateSeparator s, skipHourSeparator s with
            | Some s, _ -> parseDayOrHour (n :: prev) s
            | None, Some s -> parseMinute (n :: prev) s
            | None, None -> parseHour (n :: prev) s
        | None -> None

    let parseYearOrMonthOrDayOrHour s =
        match parseInt s with
        | Some(n, s) ->
            match skipDateSeparator s, skipHourSeparator s with
            | Some s, _ -> parseMonthOrDayOrHour [n] s
            | None, Some s -> parseMinute [n] s
            | None, None -> parseHour [n] s
        | None -> None

    parseYearOrMonthOrDayOrHour s

open System

let resolveDate (timeSpec : TimeSpec) =
    let now = DateTime.UtcNow
    let year =
        match timeSpec with
        | { Date = Some { Month = Some { Year = Some year }}} -> year
        | _ -> now.Year
    let month =
        match timeSpec with
        | { Date = Some { Month = Some { MonthOfYear = month }}} -> month
        | _ -> now.Month
    let day =
        match timeSpec with
        | { Date = Some { DayOfMonth = day }} -> day
        | _ -> now.Day
    let hour = timeSpec.Hour
    let minute = timeSpec.Minute
    DateTime(year, month, day, hour, minute, 0, DateTimeKind.Utc)