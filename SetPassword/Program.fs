﻿// Learn more about F# at http://fsharp.org
// See the 'F# Tutorial' project for more help.

open FSharp.Configuration

[<Literal>]
let sampleConfig = __SOURCE_DIRECTORY__ + @"..\..\SturmovikServerControl\SampleConfig.yaml"
type ConfigurationFile = YamlConfig<sampleConfig>

/// <summary>
/// Pick a salt, build a hash of a password, return its encoding in base64.
/// </summary>
let getPasswordHash(password : string) =
    let salt : byte[] = Array.zeroCreate 16
    use rng = new System.Security.Cryptography.RNGCryptoServiceProvider()
    rng.GetBytes(salt)
    use pbkdf2 = new System.Security.Cryptography.Rfc2898DeriveBytes(password, salt, 10000)
    let hashed = pbkdf2.GetBytes(20)
    let combined = Array.concat [salt; hashed]
    System.Convert.ToBase64String(combined)

[<EntryPoint>]
let main argv = 
    // Get path to config file where hash will be stored.
    let path =
        match argv with
        | [| "/?" |] | [| "-h" |] ->
            printfn "Usage: SetPassword <path to config.yaml>"
            System.Environment.Exit(1)
            failwith "Unreachable"
        | [| path |] ->
            path
        | [||] ->
            "config.yaml"
        | _ ->
            printfn "Bad command line"
            System.Environment.Exit(1)
            failwith "Unreachable"
    // Load config file
    let config = ConfigurationFile()
    config.Load(System.IO.Path.Combine(path))
    // Enter password, hash it
    printfn "Enter password:"
    let password = System.Console.ReadLine()
    if password.Length >= 6 then
        let hashed = getPasswordHash(password)
        config.Server.AdminPasswordHash <- hashed
        // Write config file with updated hash
        config.Save()
        printfn "Password saved."
        0
    else
        // Do not modify the file.
        printfn "Password to short, must be at least 6 chars!"
        1
