# README #

### Building the software ###

Requirements are .net 4.5, F# 4.0 and msbuild. Easiest is probably to use Visual Studio 2017.

### Running the software ###

Requires SqlServer Compact Edition 4.0.

Copy SampleConfig.yaml to config.yaml and edit to fit your needs.

Run SetPassword.exe to set the admin password.

Run SturmovikServerControl.exe.

### Live-Updating the software ###

Put a zip file called drop.zip in the directory where SturmovikServerControl.exe is located.

In the web interface, log in, then in the schedule screen, choose restart.

SturmovikServerControl.exe will start the update process, exit, and 60s later will restart.

drop.zip must have the new version of the binaries under SturmovikServerControl/bin/Release.
drop.zip must NOT have config files identically named to yours, as your files would be overwritten during the update.
The software does not check and does not warn about that.

### License ###

GNU GPL v3